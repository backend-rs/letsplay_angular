// The file contents for the current environment will overwrite these during build.
// The build system defaults to the dev environment which uses `environment.ts`, but if you do
// `ng build --env=prod` then `environment.prod.ts` will be used instead.
// The list of which env maps to which file can be found in `.angular-cli.json`.

export const environment = {
  production: false,
  silent: false,
  apiURL: {
    reports: 'http://93.188.167.68:6700/api',
    master: 'http://93.188.167.68:6700/api',
  },
  name: 'dev'
};

