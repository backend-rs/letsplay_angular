import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { HttpClient } from '@angular/common/http';

@Injectable({
    providedIn: 'root'
})
export class ApiService {
    root = environment.apiURL.reports;

    constructor(private http: HttpClient) { }


    login(model): Observable<User> {
        const subject = new Subject<User>();
        this.http.post(`${this.root}/users/login`, model).subscribe((responseData: any) => {
            if (responseData.statusCode === 200) {

                subject.next(responseData.data);
            }, (error) => {
                subject.next(error.error);

            });

        return subject.asObservable();

    }

}

