import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { Userr } from '../models/user.model';

@Injectable({
    providedIn: 'root'
})
export class ApiService {
    root = environment.apiURL.reports;
    userResponse: any;
    usersData: any;

    constructor(private http: HttpClient) { }
    login(model): Observable<Userr> {
        const subject = new Subject<Userr>();
        this.http.post(`${this.root}/users/login`, model, { headers: null }).subscribe((responseData: any) => {


            subject.next(responseData.data);
        }, (error) => {
            subject.next(error.error);

        });

        return subject.asObservable();
    }


    getUsers(): Observable<Userr[]> {
        const subject = new Subject<Userr[]>();
        this.http.get(`${this.root}/users/list?pageNo=1&pageSize=100&role=all`, { headers: null }).subscribe((responseData) => {
            this.userResponse = responseData;
            console.log('userrrrr', responseData);

            subject.next(this.userResponse.items);

        });
        return subject.asObservable();
    }

    updateUser(id, model): Observable<Userr[]> {
        const subject = new Subject<Userr[]>();
        const token = localStorage.getItem('token');

        const header = {
            headers: new HttpHeaders({
                'Content-Type': 'application/json',
                'x-access-token': token
            })

        };
        this.http.put(`${this.root}/users/update/${id}`, model, header).subscribe((responseData) => {
            this.userResponse = responseData;
            console.log('userslist', responseData);

            subject.next(this.userResponse.items);
        });
        return subject.asObservable();

    }
}

