import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { Userr } from '../models/user.model';

@Injectable({
    providedIn: 'root'
})
export class ApiService {
    root = environment.apiURL.reports;
    userResponse: any;
    usersData: any;
    responseData: any;

    constructor(private http: HttpClient) { }
    login(model): Observable<Userr> {
        const subject = new Subject<Userr>();
        this.http.post(`${this.root}/users/login`, model, { headers: null }).subscribe((responseData: any) => {


            subject.next(responseData.data);
        }, (error) => {
            subject.next(error.error);

        });

        return subject.asObservable();
    }


    getUsers(): Observable<Userr[]> {
        const subject = new Subject<Userr[]>();
        this.http.get(`${this.root}/users/list?pageNo=1&pageSize=100&role=all`, { headers: null }).subscribe((responseData) => {
            this.userResponse = responseData;
            console.log('userrrrr', this.responseData);

            subject.next(this.userResponse.items);

        });
        return subject.asObservable();
    }


}

