import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { HttpClient } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { Userr } from '../models/user.model';

@Injectable({
    providedIn: 'root'
})
export class ApiService {
    root = environment.apiURL.reports;

    constructor(private http: HttpClient) { }
    login(model): Observable<Userr> {
        const subject = new Subject<Userr>();
        this.http.post(`${this.root}/users/login`, model, { headers: null }).subscribe((responseData: any) => {


            subject.next(responseData.data);
        }, (error) => {
            subject.next(error.error);

        });

        return subject.asObservable();

    }



}

