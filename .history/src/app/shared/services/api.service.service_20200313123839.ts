import { Injectable } from '@angular/core';
import { environment } from 'environments/environment';
import { HttpClient, HttpHeaders, HttpErrorResponse } from '@angular/common/http';
@Injectable({
    providedIn: 'root'
})
export class ApiService {
    root = environment.apiURL.reports;
}

