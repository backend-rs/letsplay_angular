import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsersComponent } from './users.component';


export const UsersRoutes: Routes = [
  { path: 'Users', component: UsersComponent, data: { title: 'Users' } }
];


