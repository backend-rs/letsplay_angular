import { Routes } from '@angular/router';

import { UsersComponent } from './Users.component';

export const UsersRoutes: Routes = [
  { path: '', component: UsersComponent, data: { title: 'Users' } }
];