import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { UsersRoutes } from './users-routing.module';
import { RouterModule } from '@angular/router';
import { UsersComponent } from './Users.component';
import { MatInputModule, MatPaginatorModule, MatSortModule, MatTableModule } from '@angular/material';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

@NgModule({
  imports: [
    CommonModule,
    MatInputModule,
    MatPaginatorModule,
    MatSortModule,
    MatTableModule,
    NgxDatatableModule,
    RouterModule.forChild(UsersRoutes)
  ],
  declarations: [UsersComponent],
  // providers: [ UsersService ]

})
export class UsersModule { }
