import { Component, OnInit } from '@angular/core';
import { TablesService } from '../tables/tables.service';

@Component({
  selector: 'app-users',
  templateUrl: './users.component.html',
  styleUrls: ['./users.component.scss']
})
export class UsersComponent implements OnInit {
  rows = [];
  columns = [];

  constructor(private service: TablesService) { }


  ngOnInit() {
    this.columns = this.service.getDataConf();
    this.rows = this.service.getAll();
  }
}
