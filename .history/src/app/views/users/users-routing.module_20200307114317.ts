import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { UsersComponent } from './users.component';


export const UsersRoutes: Routes = [
  { path: 'users/list', component: UsersComponent, data: { title: 'Users' } }
];


