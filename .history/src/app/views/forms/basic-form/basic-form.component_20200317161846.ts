import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { CustomValidators } from 'ng2-validation';
import { TablesService } from 'app/views/tables/tables.service';
import { Router, ActivatedRoute } from '@angular/router';
import { DataService } from 'app/shared/services/dataservice.service';

@Component({
  selector: 'app-basic-form',
  templateUrl: './basic-form.component.html',
  styleUrls: ['./basic-form.component.css']
})
export class BasicFormComponent implements OnInit {
  formData = {};
  console = console;
  basicForm: FormGroup;

  user: any = {
    firstName: '',
    lastName: '',
    email: '',
    mobileNo: '',
    gender: '',

  };

  router: any;
  FormData: any;
  apiservice: any;
  columns = [];
  rows = [];
  submitted: boolean;
  usersData: any = {};
  responseData: any;

  constructor(private service: TablesService, public dataRoute: ActivatedRoute, private dataservice: DataService) {
    this.user = dataservice.getOption();
    console.log('dataaa', this.user);


  }

  ngOnInit() {

    // console.log('kkkkkkkkkkk', JSON.parse(this.dataRoute.snapshot.params['objectProducts']))
    // let user = JSON.parse(this.dataRoute.snapshot.params['objectProducts'])
    // tslint:disable-next-line:prefer-const
    let password = new FormControl('', Validators.required);
    // tslint:disable-next-line:prefer-const
    let confirmPassword = new FormControl('', CustomValidators.equalTo(password));

    this.basicForm = new FormGroup({
      fname: new FormControl('', [
        // Validators.minLength(4),
        // Validators.maxLength(9)
      ]),
      lname: new FormControl('', [
        // Validators.minLength(4),
        // Validators.maxLength(9)
      ]),
      age: new FormControl('', [
        // Validators.required
      ]),
      email: new FormControl('', [
        // Validators.required,
        // Validators.email
      ]),
      phoneno: new FormControl('', [
        // Validators.required
      ]),
      website: new FormControl('', CustomValidators.url),
      date: new FormControl(),
      company: new FormControl('', [
        // CustomValidators.creditCard
      ]),
      phone: new FormControl('', CustomValidators.phone('BD')),
      password: password,
      confirmPassword: confirmPassword,
      gender: new FormControl('', [
        Validators.required
      ]),
      agreed: new FormControl('', (control: FormControl) => {
        const agreed = control.value;
        if (!agreed) {
          return { agreed: true };
        }
        return null;
      })
    });
  }

  updateUser() {

    this.apiservice.updateUser(this.user.id, this.user).subscribe(res => {

      this.rows = res;

      // if (this.user.isSuccess === true) {
      alert('data updated successfully');
      this.router.navigate(['/tables/paging']);

      // } else { alert('enter valid input'); }

    });
  };






  onSubmit() {
    console.log('user', this.user);
    this.submitted = true;
    // return this.updateUser();
  }
}


