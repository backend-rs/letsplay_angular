import { Component, OnInit, ViewChild } from '@angular/core';
import { MatProgressBar, MatButton } from '@angular/material';
import { Validators, FormGroup, FormControl } from '@angular/forms';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { ApiService } from 'app/shared/services/api.service.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.css']
})
export class SigninComponent implements OnInit {
  @ViewChild(MatProgressBar, { static: false }) progressBar: MatProgressBar;
  @ViewChild(MatButton, { static: false }) submitButton: MatButton;

  signinForm: FormGroup;
  credentials = {
    username: '',
    password: '',

  };
  isLoading: boolean;
  userData: import("/home/mspl/Desktop/letsplay_angular/dist/assets/examples/material/display-value-autocomplete/display-value-autocomplete.component").User;

  constructor(private router: Router,
    private http: HttpClient,
    private apiservice: ApiService
  ) { }

  ngOnInit() {
    this.signinForm = new FormGroup({
      username: new FormControl('', Validators.required),
      password: new FormControl('', Validators.required),
      rememberMe: new FormControl(false)
    });
  }

  signin() {
    const signinData = this.signinForm.value;
    console.log(signinData);
    this.router.navigateByUrl('/dashboard/analytics');

    this.submitButton.disabled = true;
    this.progressBar.mode = 'indeterminate';
    this.apiservice.login(this.credentials).subscribe(res => {
      if (res.isSuccess === false) {
        alert('Wrong username or password!');
        return this.isLoading = false;
      } else {
        this.userData = res;

        this.isLoading = false;

        localStorage.setItem('token', this.userData.token);
        if (this.userData.res) {
          this.router.navigate(['/dashboard/analytics']);
        } else { alert('you are not admin'); }

      }

    },

    );
  };
}
