import { Component, OnInit, OnChanges } from '@angular/core';
import { TablesService } from '../tables.service';
import { Router } from '@angular/router';
import { DataService } from 'app/shared/services/dataservice.service';
import { ApiService } from 'app/shared/services/api.service.service';

@Component({
  selector: 'app-paging-table',
  templateUrl: './paging-table.component.html',
  styleUrls: ['./paging-table.component.css'],
  providers: [TablesService]
})
export class PagingTableComponent implements OnInit, OnChanges {
  columns = [];
  router: any;
  isLoading: boolean;
  usersData: any = {};
  rows = [];

  pageSize: number;
  pageNo = 1;

  constructor(private service: TablesService,
    public route: Router,
    private dataservice: DataService,
    private apiservice: ApiService) { }

  edit(data) {
    console.log('edit data', data);
    this.dataservice.setOption(data);
    // this.route.navigate(['/requests/fill', JSON.stringify(data)]);
    this.route.navigate(['forms/basic']);

  }
  getUsers() {

    this.isLoading = true;

    this.apiservice.getUsers().subscribe(res => {
      this.usersData = res;

      console.log('usr', this.usersData);

      this.isLoading = false;

    });


  }

  ngOnChanges() {
    this.columns = this.service.getDataConf();
    this.rows = this.service.getAll();

  }
  ngOnInit() {
    this.getUsers();
    this.columns = this.service.getDataConf();
    this.rows = this.service.getAll();

  }

}
