import { Component, OnInit, OnChanges } from '@angular/core';
import { TablesService } from '../tables.service';
import { Router } from '@angular/router';
import { DataService } from 'app/shared/services/dataservice.service';

@Component({
  selector: 'app-paging-table',
  templateUrl: './paging-table.component.html',
  styleUrls: ['./paging-table.component.css'],
  providers: [TablesService]
})
export class PagingTableComponent implements OnInit, OnChanges {
  rows = [];
  columns = [];
  router: any;
  isLoading: boolean;
  apiservice: any;
  usersData: any;
  pageSize: number;
  pageNo = 1;

  constructor(private service: TablesService, public route: Router, private dataservice: DataService) { }
  edit(data) {
    console.log('edit data', data);
    this.dataservice.setOption(data);
    // this.route.navigate(['/requests/fill', JSON.stringify(data)]);
    this.route.navigate(['forms/basic']);

  }
  getUsers() {

    this.isLoading = true;

    this.apiservice.getUsers().subscribe(res => {
      this.usersData = res;
      this.isLoading = false;

    });
    console.log('userrrrr', this.usersData);


  }

  ngOnChanges() {
    this.columns = this.service.getDataConf();
    this.rows = this.service.getAll();

  }
  ngOnInit() {
    this.getUsers();
    this.columns = this.service.getDataConf();
    this.rows = this.service.getAll();

  }

}
