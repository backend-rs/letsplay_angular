import { Component, OnInit } from '@angular/core';
import { TablesService } from '../tables.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-paging-table',
  templateUrl: './paging-table.component.html',
  styleUrls: ['./paging-table.component.css'],
  providers: [TablesService]
})
export class PagingTableComponent implements OnInit {
  rows = [];
  columns = [];
  router: any;

  constructor(private service: TablesService, public route: Router) { }
  edit(data) {
    console.log('edit data', data);
    this.service.setOption(data);
    // this.route.navigate(['/requests/fill', JSON.stringify(data)]);
    // this.route.navigate(['forms/basic', JSON.stringify(data)]);

  }
  ngOnInit() {
    this.columns = this.service.getDataConf();
    this.rows = this.service.getAll();
  }

}
