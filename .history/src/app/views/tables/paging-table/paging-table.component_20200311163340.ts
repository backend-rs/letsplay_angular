import { Component, OnInit } from '@angular/core';
import { TablesService } from '../tables.service';

@Component({
  selector: 'app-paging-table',
  templateUrl: './paging-table.component.html',
  styleUrls: ['./paging-table.component.css'],
  providers: [TablesService]
})
export class PagingTableComponent implements OnInit {
  rows = [];
  columns = [];
  router: any;

  constructor(private service: TablesService) { }
  edit(data) {

    this.service.setOption(data);
    this.router.navigate(['forms/basic']);

  }
  ngOnInit() {
    this.columns = this.service.getDataConf();
    this.rows = this.service.getAll();
  }

}
