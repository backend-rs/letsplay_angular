
export class Batch {

    name = '';
    startDate: any = new Date;
    endDate: any = new Date;
    startTime: any = new Date;
    endTime: any = new Date;
    // isPaid: string;
    // pricePerParticipant: string;
    // priceForSiblings: string;
    // instructor: string;
    // numberOfSeats: string;
    // location: string;

    constructor(obj?: any) {

        if (!obj) {
            return;
        }
        this.name = obj.name;
        this.startDate = obj.startDate;
        this.endDate = obj.endDate;
        this.startTime = obj.startTime;
        this.endTime = obj.endTime;
        // this.isPaid = obj.isPaid;
        // this.pricePerParticipant = obj.pricePerParticipant;
        // this.priceForSiblings = obj.priceForSiblings;
        // this.instructor = obj.instructor;
        // this.numberOfSeats = obj.numberOfSeats;
        // this.location = obj.location;
    }
}