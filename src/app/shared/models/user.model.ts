
export class Userr {
    id: string;
    _id: string;
    banners: [];
    isActivated: boolean;
    isSuccess: boolean;
    userId: string;
    firstName: string;
    lastName: string;
    addressLine1: string;
    addressLine2: string;
    emailId: string;
    about: string;
    fullName: string;
    email: string;
    type: string;
    mobile: number;
    phoneNumber: string;
    role: string;
    city: string;
    confirmPassword: string;
    password: string;
    newPassword: string;
    token: string;
    createdAt: string;
    otp: string;
    otpToken: string;
    avatarImages: string;
    category: string;
    description: string;
    facebook: string;
    website: string;
    twitter: string;
    instagram: string;
    youtube: string;
    country: string;
    zipCode: string;
    lat: string;
    long: string;
    answer: string;
    securityQuestion: string;

    providerId: string;
    programId: string;
    status: string;

    taxNumber: string;
    sex: string;
    stripeToken: string;
    stripeKey: string;
    ssn: string;
    deviceToken: string;


    constructor(obj?: any) {

        if (!obj) {
            return;
        }

        this.id = obj.id;
        this._id = obj._id;
        this.banners = obj.banners;
        this.isActivated = obj.isActivated;
        this.isSuccess = obj.isSuccess;
        this.fullName = obj.fullName;
        this.firstName = obj.firstName;
        this.lastName = obj.lastName;
        this.about = obj.about;
        this.emailId = obj.emailId;
        this.email = obj.email;
        this.addressLine1 = obj.addressLine1;
        this.addressLine2 = obj.addressLine2;
        this.type = obj.type;
        this.mobile = obj.mobile;
        this.phoneNumber = obj.phoneNumber;
        this.password = obj.password;
        this.newPassword = obj.newPassword;
        this.confirmPassword = obj.confirmPassword;
        this.city = obj.city;
        this.token = obj.token;
        this.createdAt = obj.createdAt;
        this.otp = obj.otp;
        this.otpToken = obj.otpToken;
        this.avatarImages = obj.avatarImages;
        this.category = obj.category;
        this.description = obj.description;
        this.facebook = obj.facebook;
        this.youtube = obj.youtube;
        this.website = obj.website;
        this.twitter = obj.twitter;
        this.instagram = obj.instagram;
        this.country = obj.country;
        this.zipCode = obj.zipCode;
        this.lat = obj.lat;
        this.long = obj.long;
        this.securityQuestion = obj.securityQuestion;
        this.answer = obj.answer;

        this.providerId = obj.providerId;
        this.programId = obj.programId;
        this.status = obj.status;

        this.taxNumber = obj.taxNumber;
        this.sex = obj.sex;
        this.stripeToken = obj.stripeToken;
        this.stripeKey = obj.stripeKey;
        this.ssn = obj.ssn;
        this.deviceToken = obj.deviceToken;



    }
}