import { Injectable } from '@angular/core';

@Injectable({
    providedIn: 'root'
})
export class DataService {

    private data: any;

    setOption(option) {
        console.log('setOption', option);
        this.data = option;
        console.log('data from data service = >> ', this.data);

    }

    getOption() {
        console.log('getOption', this.data);
        return this.data;
    }


    constructor() { }
}
