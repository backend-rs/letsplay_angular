import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable, Subject } from 'rxjs';
import { Userr } from '../models/user.model';
import { Category } from '../models/category.model';
import { Tag } from '../models/tag.model';
import { Child } from '../models/child.model';
import { Program } from '../models/program.model';
import { Claim } from '../models/claim.model';
import { environment } from 'environments/environment.prod';

@Injectable({
    providedIn: 'root'
})
export class ApiService {
    root = environment.apiURL;
    userResponse: any;
    usersData: any;
    categoryResponse: any;
    tagResponse: any;
    token: string = ''



    constructor(private http: HttpClient) {
        this.token = localStorage.getItem('token');
    }
    // --------------------access token------------------------
    getHeaders() {
        let header
        if (this.token != '') {
            header = {
                headers: new HttpHeaders({
                    'Content-Type': 'application/json',
                    'x-access-token': this.token
                })
            }
        } else {
            console.log('token not found')
        }
        return header;

    }
    getImageHeader() {
        let header
        if (this.token != '') {
            header = {
                headers: new HttpHeaders({
                    'enctype': 'multipart/form-data',
                    'Accept': 'application/json',
                    'x-access-token': this.token
                })
            }
        } else {
            console.log('token not found')
        }
        return header;

    }
    // header = {
    //     headers: new HttpHeaders({
    //         'Content-Type': 'application/json',
    //         'x-access-token': this.token
    //     })

    // };

    login(model): Observable<Userr> {
        const subject = new Subject<Userr>();
        this.http.post(`${this.root}/users/login`, model, { headers: null }).subscribe((responseData: any) => {
            subject.next(responseData);
        }, (error) => {
            subject.next(error.error);

        });

        return subject.asObservable();
    }
    getPicUrl(pic) {
        const subject = new Subject<any>();
        this.http.post(`${this.root}/uploads/getPicUrl`, pic, this.getImageHeader()).subscribe((response) => {
            subject.next(response);
            console.log('response', response)
        }, (error) => {
            console.log('error', error)
            subject.next(error.error);
        }
        );
        return subject.asObservable();

    }

    addCategory(model): Observable<Category[]> {
        const subject = new Subject<Category[]>();
        this.http.post(`${this.root}/categories/add`, model, this.getHeaders()).subscribe((responseData) => {
            this.categoryResponse = responseData;
            subject.next(this.categoryResponse);
            console.log('add category response', this.categoryResponse);
            console.log('response', responseData)
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();

    }


    addTag(model): Observable<Tag[]> {
        const subject = new Subject<Tag[]>();
        this.http.post(`${this.root}/tags/add`, model, this.getHeaders()).subscribe((responseData) => {
            this.tagResponse = responseData;
            subject.next(this.tagResponse);
            console.log('add category response', this.tagResponse);
            console.log('response', responseData)
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();

    }
    getCategory(): Observable<Category[]> {
        const subject = new Subject<Category[]>();
        this.http.get(`${this.root}/categories/list`, this.getHeaders()).subscribe((responseData) => {
            this.categoryResponse = responseData;
            console.log('get category API=>>', responseData);
            subject.next(this.categoryResponse.data);

        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    getTag(): Observable<Tag[]> {
        const subject = new Subject<Tag[]>();
        this.http.get(`${this.root}/tags/list`, this.getHeaders()).subscribe((responseData) => {
            this.tagResponse = responseData;
            console.log('get category API=>>', responseData);
            subject.next(this.tagResponse.data);

        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    getUsers(data): Observable<Userr[]> {
        const subject = new Subject<Userr[]>();
        this.http.get(`${this.root}/users/list?pageNo=1&pageSize=1000&role=${data}`, { headers: null }).subscribe((responseData) => {
            this.userResponse = responseData;
            subject.next(this.userResponse.items);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    userActiveInActive(id, isActivated) {
        const subject = new Subject<Userr[]>();
        let m;
        this.http.put(`${this.root}/users/activeOrDeactive?id=${id}&isActivated=${isActivated}`, m, this.getHeaders()).subscribe(res => {
            this.userResponse = res;
            console.log('response from server =>> ', this.userResponse);
            subject.next(this.userResponse);
        }, error => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }

    getUserById(id): Observable<Userr[]> {
        const subject = new Subject<Userr[]>();
        this.http.get(`${this.root}/users/getById/${id}`, this.getHeaders()).subscribe((responseData) => {
            this.userResponse = responseData;
            subject.next(this.userResponse.data);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    uploadUserImage(id, model): Observable<any> {
        const subject = new Subject<any>();
        this.http.put(`${this.root}/users/uploadProfilePic/${id}`, model, this.getImageHeader()).subscribe((response) => {
            // this.tagResponse = response;
            subject.next(response);
            // console.log('add category response', this.tagResponse);
            console.log('response', response)
        }, (error) => {
            console.log('error', error)
            subject.next(error.error);
        }
        );
        return subject.asObservable();

    }
    addUser(data): Observable<Userr[]> {
        const subject = new Subject<Userr[]>();
        this.http.post(`${this.root}/users/register`, data, { headers: null }).subscribe(res => {
            this.userResponse = res;
            console.log('response from server =>> ', this.userResponse);
            subject.next(this.userResponse);

        }, error => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    getParents(): Observable<Userr[]> {
        const subject = new Subject<Userr[]>();
        this.http.get(`${this.root}/parents/list?pageNo=1&pageSize=1000`, { headers: null }).subscribe((responseData) => {
            this.userResponse = responseData;
            subject.next(this.userResponse.items);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    deleteUser(id): Observable<Userr[]> {
        const subject = new Subject<Userr[]>();
        let a;
        this.http.put(`${this.root}/users/delete?id=${id}`, a, this.getHeaders()).subscribe((responseData) => {
            this.userResponse = responseData;
            subject.next(this.userResponse);

        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }

    getChild(): Observable<Child[]> {
        const subject = new Subject<Child[]>();
        this.http.get(`${this.root}/child/list`, this.getHeaders()).subscribe((responseData) => {
            this.userResponse = responseData;
            subject.next(this.userResponse.items);

        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    deleteChild(id): Observable<Child[]> {
        var response: any;
        let a;
        const subject = new Subject<Child[]>();
        this.http.put(`${this.root}/child/delete/${id}`, a, this.getHeaders()).subscribe((responseData) => {
            response = responseData;
            console.log('res from server >>>', response);
            subject.next(response);

        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }


    getChildByParentId(id): Observable<Child[]> {
        const subject = new Subject<Child[]>();
        this.http.get(`${this.root}/child/byParentId/${id}`, this.getHeaders()).subscribe(res => {
            this.userResponse = res;
            console.log('response from server =>> ', this.userResponse);
            subject.next(this.userResponse.data);

        }, error => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }

    addChild(model): Observable<Child[]> {
        const subject = new Subject<Child[]>();
        this.http.post(`${this.root}/child/add`, model, this.getHeaders()).subscribe(res => {
            this.userResponse = res;
            console.log('response from server =>> ', this.userResponse);
            subject.next(this.userResponse);

        }, error => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }


    updateChild(id, model): Observable<Child[]> {
        const subject = new Subject<Child[]>();
        this.http.put(`${this.root}/child/update/${id}`, model, this.getHeaders()).subscribe(res => {
            this.userResponse = res;
            console.log('response from server =>> ', this.userResponse);
            subject.next(this.userResponse);

        }, error => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }

    updateParent(_id, model): Observable<Userr[]> {
        const subject = new Subject<Userr[]>();
        this.http.put(`${this.root}/parents/update/${_id}`, model, this.getHeaders()).subscribe((responseData) => {
            this.userResponse = responseData;
            subject.next(this.userResponse);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();

    }

    updateCategory(_id, model): Observable<Category[]> {
        const subject = new Subject<Category[]>();
        this.http.put(`${this.root}/categories/update/${_id}`, model, this.getHeaders()).subscribe((responseData) => {
            this.categoryResponse = responseData;
            console.log('categoryupdate rwsponse>>>>>>', responseData);

            subject.next(this.categoryResponse);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();

    }

    updateTag(_id, model): Observable<Tag[]> {
        const subject = new Subject<Tag[]>();
        this.http.put(`${this.root}/tags/update/${_id}`, model, this.getHeaders()).subscribe((responseData) => {
            this.tagResponse = responseData;
            // console.log('userslist', responseData);

            subject.next(this.tagResponse);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();

    }

    searchTag(key): Observable<Tag> {
        const subject = new Subject<Tag>();
        console.log('key', key)
        this.http.get(`${this.root}/tags/search?name=${key}`, this.getHeaders()).subscribe((responseData: any) => {

            subject.next(responseData.data);
        }, (error) => {
            subject.next(error.error);

        });

        return subject.asObservable();
    }

    addProgram(model): Observable<Program[]> {
        const subject = new Subject<Program[]>();
        this.http.post(`${this.root}/programs/add`, model, this.getHeaders()).subscribe(res => {
            this.userResponse = res;
            console.log('response from server =>> ', this.userResponse);
            subject.next(this.userResponse);
        }, error => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }


    updateProgram(id, model): Observable<Program[]> {
        const subject = new Subject<Program[]>();
        this.http.put(`${this.root}/programs/update/${id}`, model, this.getHeaders()).subscribe(res => {
            this.userResponse = res;
            console.log('response from server =>> ', this.userResponse);
            subject.next(this.userResponse);

        }, error => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }


    getProgram(pageNo, pageSize): Observable<Program> {
        const subject = new Subject<Program>();
        this.http.get(`${this.root}/programs/list?pageNo=${pageNo}&pageSize=${pageSize}`, this.getHeaders()).subscribe((responseData: any) => {
            subject.next(responseData.items);
        }, (error) => {
            subject.next(error.error);
        });

        return subject.asObservable();
    }
    deleteProgram(id): Observable<Program[]> {
        const subject = new Subject<Program[]>();
        let a;
        this.http.delete(`${this.root}/programs/delete/${id}`, this.getHeaders()).subscribe((responseData) => {
            this.userResponse = responseData;
            subject.next(this.userResponse);

        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
    getProgramByProvider(userId, pageNo, pageSize): Observable<Program> {

        // if (!this.token) {
        //     this.getToken
        // }
        console.log('TOKEN', this.token)
        const subject = new Subject<Program>();
        this.http.get(`${this.root}/programs/byProvider?userId=${userId}&pageNo=${pageNo}&pageSize=${pageSize}`, this.getHeaders()).subscribe((responseData: any) => {
            subject.next(responseData.data);
        }, (error) => {
            console.log('error', error)
            subject.next(error);
        });
        return subject.asObservable();
    }

    programActiveInActive(id, status) {
        const subject = new Subject<Program[]>();
        let m;
        this.http.put(`${this.root}/programs/activeOrDecactive?id=${id}&status=${status}`, m, this.getHeaders()).subscribe(res => {
            this.userResponse = res;
            console.log('response from server =>> ', this.userResponse);
            subject.next(this.userResponse);
        }, error => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }


    getProvider(): Observable<Program> {
        const subject = new Subject<Program>();
        this.http.get(`${this.root}/providers/list`, this.getHeaders()).subscribe((responseData: any) => {
            subject.next(responseData.data);
        }, (error) => {
            subject.next(error.error);
        });

        return subject.asObservable();
    }

    updateProvider(model): Observable<Userr[]> {
        const subject = new Subject<Userr[]>();
        this.http.put(`${this.root}/providers/update/${model.id}`, model, this.getHeaders()).subscribe((responseData) => {
            this.userResponse = responseData;
            console.log('response from server =>> ', this.userResponse);
            subject.next(this.userResponse);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();

    }
    getClaimsList(): Observable<Claim[]> {
        const subject = new Subject<Claim[]>();
        this.http.get(`${this.root}/claims/requestList`, this.getHeaders()).subscribe((responseData) => {
            this.userResponse = responseData;
            console.log('claims response>>>>>>', this.userResponse);

            subject.next(this.userResponse.data);
        }, (error) => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }

    claimAction(data): Observable<Claim[]> {
        const subject = new Subject<Claim[]>();
        this.http.put(`${this.root}/claims/action/${data._id}`, data, this.getHeaders()).subscribe(res => {
            this.userResponse = res;
            console.log('response from server =>> ', this.userResponse);
            subject.next(this.userResponse);

        }, error => {
            subject.next(error.error);
        });
        return subject.asObservable();
    }
}

