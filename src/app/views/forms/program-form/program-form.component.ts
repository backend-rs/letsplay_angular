import { Component, OnInit } from '@angular/core';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { TablesService } from 'app/views/tables/tables.service';
import { ActivatedRoute, Router } from '@angular/router';
import { DataService } from 'app/shared/services/dataservice.service';
import { ApiService } from 'app/shared/services/api.service.service';
import { CustomValidators } from 'ng2-validation';
import { AppLoaderService } from 'app/shared/services/app-loader/app-loader.service';

@Component({
  selector: 'app-program-form',
  templateUrl: './program-form.component.html',
  styleUrls: ['./program-form.component.scss']
})
export class ProgramFormComponent implements OnInit {

  formData = {};
  programForm: FormGroup;

  user: any = {
    firstName: '',
    lastName: '',
    email: '',
    mobileNo: '',
    gender: '',

  };


  FormData: any;
  submitted: boolean;
  usersData: any = {};
  responseData: any;
  res: any[];



  constructor(private service: TablesService,
    public dataRoute: ActivatedRoute,
    private dataservice: DataService,
    private apiservice: ApiService,
    private loader: AppLoaderService,
    private route: Router) {
    this.user = dataservice.getOption();
    console.log('dataaa', this.user);


  }
  back() {
    this.route.navigate(['profile/program']);
  }
  ngOnInit() {

    // console.log('kkkkkkkkkkk', JSON.parse(this.dataRoute.snapshot.params['objectProducts']))
    // let user = JSON.parse(this.dataRoute.snapshot.params['objectProducts'])
    // tslint:disable-next-line:prefer-const
    // let password = new FormControl('', Validators.required);
    // tslint:disable-next-line:prefer-const
    // let confirmPassword = new FormControl('', CustomValidators.equalTo(password));

    this.programForm = new FormGroup({
      fname: new FormControl('', []),
      lname: new FormControl('', []),
      age: new FormControl('', []),
      email: new FormControl('', []),
      phoneno: new FormControl('', []),
      sex: new FormControl('', []),
    });
  }

  updateParent() {
    this.loader.open();
    this.apiservice.updateParent(this.user.id, this.user).subscribe(res => {
      this.loader.close();
      console.log('resssss', res);
      // if (this.user.isSuccess === true) {
      alert('data updated successfully');
      // this.router.navigate(['tables/paging']);
      this.route.navigate(['tables/paging']);
      //    else { alert('enter valid input'); }
      // }
    });
  };



  onSubmit() {

    this.submitted = true;

    console.log('user', this.user);
    return this.updateParent();
  }
}

