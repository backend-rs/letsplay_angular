import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProgramFormRoutingModule } from './program-form-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ProgramFormRoutingModule
  ]
})
export class ProgramFormModule { }
