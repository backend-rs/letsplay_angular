import { Component, OnInit } from '@angular/core';
import { FormGroup, Validators, FormControl } from '@angular/forms';
import { TablesService } from 'app/views/tables/tables.service';
import { ActivatedRoute, Router } from '@angular/router';
import { ApiService } from 'app/shared/services/api.service.service';
import { DataService } from 'app/shared/services/dataservice.service';
import { CustomValidators } from 'ng2-validation';
import { MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition, MatSnackBar, MatSnackBarConfig } from '@angular/material';
import { Userr } from 'app/shared/models/user.model';
import { AppLoaderService } from 'app/shared/services/app-loader/app-loader.service';

@Component({
  selector: 'app-provider-update-form',
  templateUrl: './provider-update-form.component.html',
  styleUrls: ['./provider-update-form.component.css']
})
export class ProviderUpdateFormComponent implements OnInit {


  formData = {};
  providerUpdateForm: FormGroup;

  user = new Userr;


  FormData: any;
  submitted: boolean;
  usersData: any = {};
  responseData: any;
  res: any[];
  providerResponse: any;
  message: string = 'Provider Updated Successfully !';
  // actionButtonLabel: string = 'Retry';
  action: boolean = true;
  setAutoHide: boolean = true;
  autoHide: number = 4000;
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'bottom';

  // addExtraClass: boolean = false;


  constructor(private service: TablesService,
    public dataRoute: ActivatedRoute,
    private dataservice: DataService,
    private apiservice: ApiService,
    private snack: MatSnackBar,
    private loader: AppLoaderService,
    private route: Router) {
    this.user = dataservice.getOption();
  }
  back() {
    this.route.navigate(['tables/provider']);
  }

  updateProvider() {
    this.loader.open();
    this.apiservice.updateProvider(this.user).subscribe(res => {
      this.providerResponse = res;
      this.loader.close();
      console.log('parentResponse from serve = >>> ', this.providerResponse);
      if (this.providerResponse.isSuccess === true) {
        this.snack.open(this.message, 'OK', { duration: 4000 })
        this.route.navigate(['tables/provider']);
      } else {
        let msg = "Something Went Wrong";
        this.snack.open(msg, 'OK', { duration: 4000 });
      }
    });
  }


  ngOnInit() {

    // console.log('kkkkkkkkkkk', JSON.parse(this.dataRoute.snapshot.params['objectProducts']))
    // let user = JSON.parse(this.dataRoute.snapshot.params['objectProducts'])
    // tslint:disable-next-line:prefer-const
    // let password = new FormControl('', Validators.required);
    // tslint:disable-next-line:prefer-const
    // let confirmPassword = new FormControl('', CustomValidators.equalTo(password));


    this.providerUpdateForm = new FormGroup({
      firstName: new FormControl('', Validators.required),
      lastName: new FormControl('',),
      email: new FormControl('', [Validators.required, Validators.email]),
      phoneNumber: new FormControl('',),
      sex: new FormControl('', Validators.required),
      addressLine1: new FormControl('',),
      addressLine2: new FormControl('',),
      city: new FormControl('',),
      facebook: new FormControl('',),
      website: new FormControl('',),
      instagram: new FormControl('',),
      twitter: new FormControl('',),
      taxNumber: new FormControl('',),
      country: new FormControl('',),
      zipCode: new FormControl('',),
      lat: new FormControl('',),
      long: new FormControl('',),
      stripeToken: new FormControl('',),
      stripeKey: new FormControl('',),
      ssn: new FormControl('',),
      deviceToken: new FormControl('',),

      // date: new FormControl(),
      // company: new FormControl('', [
      //    CustomValidators.creditCard
      // ]),
      // phone: new FormControl('', CustomValidators.phone('BD')),
      // password: password,
      // confirmPassword: confirmPassword,
      // gender: new FormControl('', [
      //   Validators.required
      // ]),
      // agreed: new FormControl('', (control: FormControl) => {
      //   const agreed = control.value;
      //   if (!agreed) {
      //     return { agreed: true };
      //   }
      //   return null;
      // })
    });
  }





  onSubmit() {

    this.submitted = true;

    console.log('user', this.user);
    return this.updateProvider();
  }
}

