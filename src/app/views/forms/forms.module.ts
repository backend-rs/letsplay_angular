import { NgModule } from '@angular/core';
import { RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import {
  MatInputModule,
  MatDatepickerModule,
  MatNativeDateModule,
  MatListModule,
  MatCardModule,
  MatProgressBarModule,
  MatRadioModule,
  MatCheckboxModule,
  MatButtonModule,
  MatIconModule,
  MatStepperModule,
  MatOptionModule,
  MatSelectModule,
  MatSnackBarModule,
  MatChipsModule,
  MatAutocompleteModule,
  MatRippleModule,
  MatButtonToggleModule,
  MatSlideToggleModule,
  MatSliderModule,
  MatDialogModule
} from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { QuillModule } from 'ngx-quill';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { FileUploadModule } from 'ng2-file-upload/ng2-file-upload';

import { BasicFormComponent } from './basic-form/basic-form.component';
import { RichTextEditorComponent } from './rich-text-editor/rich-text-editor.component';
import { FileUploadComponent } from './file-upload/file-upload.component';

import { FormsRoutes } from './forms.routing';
import { WizardComponent } from './wizard/wizard.component';
import { ParentFormComponent } from './parent-form/parent-form.component';
import { ProviderFormComponent } from './provider-form/provider-form.component';
import { CategoryFormComponent } from './category-form/category-form.component';
import { ChildFormComponent } from './child-form/child-form.component';
import { ProgramFormComponent } from './program-form/program-form.component';
import { AdminFormComponent } from './admin-form/admin-form.component';
import { AddBatchPopupComponent } from './wizard/add-batch-popup/add-batch-popup.component';
import { EditProgramComponent } from './edit-program/edit-program.component';
import { AddcategoryFormComponent } from './addcategory-form/addcategory-form.component';
import { ParentUpdateFormComponent } from './parent-update-form/parent-update-form.component';
import { ProviderUpdateFormComponent } from './provider-update-form/provider-update-form.component';
import { Ng5SliderModule } from 'ng5-slider';
import { AutocompleteLibModule } from 'angular-ng-autocomplete';
@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    MatSelectModule,
    MatOptionModule,
    FormsModule,
    MatInputModule,
    MatListModule,
    MatCardModule,
    MatDatepickerModule,
    MatNativeDateModule,
    MatProgressBarModule,
    MatRadioModule,
    MatCheckboxModule,
    Ng5SliderModule,
    AutocompleteLibModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    MatButtonModule,
    MatIconModule,
    MatStepperModule,
    FlexLayoutModule,
    QuillModule,
    MatSnackBarModule,
    NgxDatatableModule,
    FileUploadModule,
    MatChipsModule,
    MatAutocompleteModule,
    MatRippleModule,
    MatButtonToggleModule,
    MatSlideToggleModule,
    MatSliderModule,
    MatDialogModule,
    RouterModule.forChild(FormsRoutes)
  ],
  declarations: [BasicFormComponent,
    EditProgramComponent,
    RichTextEditorComponent,
    FileUploadComponent,
    WizardComponent,
    ParentFormComponent,
    ParentUpdateFormComponent,
    ProviderFormComponent,
    ProviderUpdateFormComponent,
    CategoryFormComponent,
    AddcategoryFormComponent,
    ChildFormComponent,
    ProgramFormComponent,
    AdminFormComponent,
    AddBatchPopupComponent],
  providers: [],
  entryComponents: [AddBatchPopupComponent]
})
export class AppFormsModule { }