import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { AdminFormRoutingModule } from './admin-form-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AdminFormRoutingModule
  ]
})
export class AdminFormModule { }
