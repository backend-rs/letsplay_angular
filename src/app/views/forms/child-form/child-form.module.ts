import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ChildFormRoutingModule } from './child-form-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ChildFormRoutingModule
  ]
})
export class ChildFormModule { }
