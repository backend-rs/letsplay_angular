import { Component, OnInit, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { Batch } from 'app/shared/models/batch.model';
import * as moment from 'moment';
import { DataService } from 'app/shared/services/dataservice.service';

@Component({
  selector: 'add-batch-popup',
  templateUrl: './add-batch-popup.component.html'
})
export class AddBatchPopupComponent implements OnInit {
  public itemForm: FormGroup;
  tags: any = [];
  tag: any = [];
  batches: any = [];
  startDate = new Date;
  endDate = new Date;
  batch = new Batch;
  constructor(
    @Inject(MAT_DIALOG_DATA) public data: any,
    public dialogRef: MatDialogRef<AddBatchPopupComponent>,
    private fb: FormBuilder,
    private dataservice: DataService,
  ) { }

  addBatch() {

    this.batches.push(this.batch);
    this.batch = new Batch;
    console.log('program batches', this.batches);
    this.dataservice.setOption(this.batches);
  }

  ngOnInit() {
    let data = this.dataservice.getOption();
    if (data) {
      this.batches = data
    }

    console.log('batch', data)
    // this.buildItemForm(this.data.payload)
  }
  // buildItemForm(item) {
  //   this.itemForm = this.fb.group({
  //     name: [item.name || '', Validators.required],
  //     age: [item.age || ''],
  //     email: [item.email || ''],
  //     company: [item.company || ''],
  //     phone: [item.phone || ''],
  //     address: [item.address || ''],
  //     balance: [item.balance || ''],
  //     isActive: [item.isActive || false]
  //   })
  // }
  submit() {
    this.dialogRef.close();
    this.batch.startDate = moment(this.startDate).format('MM-DD-YYYY');
    this.batch.endDate = moment(this.endDate).format("MM-DD-YYYY");
    this.addBatch();

    // or
    // this.dialogRef.close(this.itemForm.value)
  }
}
