import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ENTER, COMMA } from '@angular/cdk/keycodes';
import { Observable } from 'rxjs';
import { MatAutocomplete, MatAutocompleteSelectedEvent, MatChipInputEvent, MatSliderChange, MatDialogRef, MatDialog, MatSnackBarConfig, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition, MatSnackBar } from '@angular/material';
import { startWith, map } from 'rxjs/operators';
import { FileUploader } from 'ng2-file-upload';
import { AddBatchPopupComponent } from './add-batch-popup/add-batch-popup.component';
import { ActivatedRoute, Router } from '@angular/router';
import { Userr } from 'app/shared/models/user.model';
import { Program } from 'app/shared/models/program.model';
import { Options } from 'ng5-slider';
import { ApiService } from 'app/shared/services/api.service.service';
import * as moment from 'moment';
import { AppLoaderService } from 'app/shared/services/app-loader/app-loader.service';
import { DataService } from 'app/shared/services/dataservice.service';

@Component({
  selector: 'app-wizard',
  templateUrl: './wizard.component.html',
  styleUrls: ['./wizard.component.css']
})
export class WizardComponent implements OnInit {
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  fourthFormGroup: FormGroup;
  startDate = new Date;
  endDate = new Date;
  startTime = new Date;
  endTime = new Date;

  imgResponse: any;
  fileData: File = null;
  msg: string;
  imagePath;
  imgURL: any;


  bookingCancelledIn = {
    days: "",
    hours: ""
  };
  time = {
    from: moment(this.startDate).format('MM-DD-YYYY'),
    to: moment(this.endDate).format('MM-DD-YYYY'),
  };
  date = {
    from: new Date(this.startDate),
    to: new Date(this.endDate)
  };
  user = new Userr;
  program = new Program;
  batches: any = {};

  //  ng5slider start age group

  minAge: number = 3;
  maxAge: number = 10;

  ageOption: Options = {
    floor: 0,
    ceil: 100,
    translate: (value: number): string => {
      return value + ' YRS';
    }

  };

  //  ng5slider start capacity

  minCapacity: number = 0;
  maxCapacity: number = 30;

  capacityOption: Options = {
    floor: 0,
    ceil: 100,
    translate: (value: number): string => {
      return value + '';
    }

  };
  // ng5slider end


  // ---------------autucomplete-------------  
  visible: boolean = true;
  selectable: boolean = true;
  removable: boolean = true;
  addOnBlur: boolean = false;
  keyword = 'name';

  separatorKeysCodes: number[] = [ENTER, COMMA];

  fruitCtrl = new FormControl();

  filteredFruits: Observable<any[]>;
  filteredValues: Observable<any[]>;

  tags: any = [];
  tag: any = [];
  // batches: any = [];
  // ng5slider end

  public uploader: FileUploader = new FileUploader({ url: 'upload_url' });
  public hasBaseDropZoneOver: boolean = false;

  action: boolean = true;
  setAutoHide: boolean = true;
  autoHide: number = 4000;
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'bottom';

  constructor(private fb: FormBuilder,
    private apiservice: ApiService,
    private dialog: MatDialog,
    private loader: AppLoaderService,
    private activatedRoute: ActivatedRoute,
    private route: Router,
    private dataservice: DataService,
    private snack: MatSnackBar) {
    this.activatedRoute.params.subscribe(params => {
      this.user._id = params['id'];
    });
    // this.batches = dataservice.getOption();

    let config = new MatSnackBarConfig();
    config.verticalPosition = this.verticalPosition;
    config.horizontalPosition = this.horizontalPosition;
    config.duration = this.setAutoHide ? this.autoHide : 0;
  }
  openPopUp() {
    let dialogRef: MatDialogRef<any> = this.dialog.open(AddBatchPopupComponent, {
      width: '30%',
      disableClose: true,
      // data: this.batches
    })
    dialogRef.afterClosed()
      .subscribe(res => {
        if (!res) {
          // If user press cancel
          return;
        }
        // this.loader.open();


      });
  }
  onFileChanged(e) { }

  onAgeChange(event: MatSliderChange) {
    console.log("This is emitted as the thumb slides");
    console.log(event.value);
  }

  remove(fruit, indx): void {
    this.tag.splice(indx, 1);
    console.log('tagsssss', this.tag);
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.tag.push(event.option.value);
  }



  selectEvent(item) {
    this.tag.push(item)
    console.log('selectEvent', item);
    console.log('intrests', this.tag);
  }

  onChangeSearch(key: string) {
    this.tags = []
    this.apiservice.searchTag(key).subscribe((res: any) => {
      this.tags = res;
      console.log('searchTag list categories', this.tags);
    });

    // fetch remote data from here
    // And reassign the 'data' which is binded to 'data' property.
  }

  onFocused(e) {
    console.log('onFocused', e)
    // do something when input is focused
  }

  ngOnInit() {
    this.firstFormGroup = new FormGroup({
      type: new FormControl(['',]),
    });
    this.secondFormGroup = new FormGroup({
      name: new FormControl(['',]),
      description: new FormControl(['',]),
      startDate: new FormControl(['',]),
      endDate: new FormControl(['',]),
      startTime: new FormControl(['',]),
      endTime: new FormControl(['',]),
      isFree: new FormControl(false),
      category: new FormControl(['',]),
      ageGroup: new FormControl(['',]),
      pricePerParticipant: new FormControl(['',]),
      priceForSiblings: new FormControl(['',]),



      // startDate: ['',],
      // endDate: ['',],
      // startTime: ['',],
      // endTime: ['',],
      // associates: ['',],
      // category: ['',],
      // ageGroup: [,],
      // imageUplad: [,],
      // isProgramFree: [true,],
      // pricePerParticipant: ['',],
      // priceForSiblings: ['',],




    });
    this.thirdFormGroup = new FormGroup({
      email: new FormControl(['', Validators.email]),
      specialInstructions: new FormControl(['',]),
      adultAssistanceIsRequried: new FormControl(false),
      addresses: new FormControl(['',]),
      days: new FormControl(['',]),
      hours: new FormControl(['',]),


      // email: ['',],
      // special_instruction: ['',],
      // isAdult_assistance: [true,],
      // booking_cancle_before: ['',],
      // address: ['',],

    });

  }

  // public fileOverBase(e: any): void {
  //   this.hasBaseDropZoneOver = e;
  // }

  fileSelect(event) {
    let formData = new FormData();
    this.fileData = event.target.files[0];
    formData.append('image', this.fileData);
    this.loader.open();
    this.apiservice.getPicUrl(formData).subscribe(res => {
      this.imgResponse = res;
      this.loader.close();
      console.log('IMAGE Response from server = >>> ', this.imgResponse);
      if (this.imgResponse.isSuccess === true) {
        this.program.programCoverPic = this.imgResponse.data;


      } else {
        let msg = "Something Went Wrong!";
        this.snack.open(msg, 'OK', { duration: 4000 });

      }
    });

    // --------------------preview image before upload ------------------------

    if (event.target.files.length === 0)
      return;
    var reader = new FileReader();
    this.imagePath = event.target.files;
    reader.readAsDataURL(event.target.files[0]);
    reader.onload = (_event) => {
      this.imgURL = reader.result;
    }
    var mimeType = event.target.files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.msg = " only images are supported";
      return;
    }
    // -------------------------------------------------------------------------------

  }

  addProgram() {
    var response: any;
    this.batches = this.dataservice.getOption();
    this.program.capacity.min = this.minCapacity
    this.program.capacity.max = this.maxCapacity
    this.program.ageGroup.from = this.minAge
    this.program.ageGroup.to = this.maxAge
    this.program.userId = this.user._id;
    this.program.bookingCancelledIn = this.bookingCancelledIn;
    this.program.time = this.time;
    this.program.batches = this.batches;
    this.program.date.from = moment(this.startDate).format('MM-DD-YYYY')
    this.program.date.to = moment(this.endDate).format("MM-DD-YYYY")
    // this.program.batches = this.batches;
    this.program.tags = this.tag;

    console.log('program data before upload', this.program);

    this.loader.open();
    this.apiservice.addProgram(this.program).subscribe(res => {
      response = res
      this.loader.close();
      if (response.isSuccess === true) {
        // this.snack.open('Program Added successfully', 'OK', { duration: 5000 });
        this.route.navigate(['tables/program', this.user._id]);
      } else {
        // this.router.navigate(["/program/list"]);
        // this.snack.open('Program Added successfully', 'OK', { duration: 5000 });
      }
    });

  }

  submit() {
    // console.log(this.firstFormGroup.value);
    // console.log(this.secondFormGroup.value);
    // console.log(this.thirdFormGroup.value);
    this.addProgram();
  }
}
