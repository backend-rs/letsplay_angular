import { Routes } from '@angular/router';

import { BasicFormComponent } from './basic-form/basic-form.component';
import { RichTextEditorComponent } from './rich-text-editor/rich-text-editor.component';
import { FileUploadComponent } from './file-upload/file-upload.component';
import { WizardComponent } from './wizard/wizard.component';
import { ProviderFormComponent } from './provider-form/provider-form.component';
import { ParentFormComponent } from './parent-form/parent-form.component';
import { CategoryFormComponent } from './category-form/category-form.component';
import { ChildFormComponent } from './child-form/child-form.component';
import { ProgramFormComponent } from './program-form/program-form.component';
import { AdminFormComponent } from './admin-form/admin-form.component';
import { EditProgramComponent } from './edit-program/edit-program.component';
import { AddcategoryFormComponent } from './addcategory-form/addcategory-form.component';
import { ParentUpdateFormComponent } from './parent-update-form/parent-update-form.component';
import { ProviderUpdateFormComponent } from './provider-update-form/provider-update-form.component';

export const FormsRoutes: Routes = [
  {
    path: '',
    children: [{
      path: 'basic',
      component: BasicFormComponent,
      data: { title: '', breadcrumb: 'BASIC' }
    }, {
      path: 'editor',
      component: RichTextEditorComponent,
      data: { title: 'Editor', breadcrumb: 'EDITOR' }
    },
    {
      path: 'upload',
      component: FileUploadComponent,
      data: { title: 'Upload', breadcrumb: 'UPLOAD' }
    }, {
      path: 'wizard/:id',
      component: WizardComponent,
      data: { title: 'Wizard', breadcrumb: 'WIZARD' }
    },
    {
      path: 'edit-program/:id',
      component: EditProgramComponent,
      data: { title: 'Edit-program', breadcrumb: 'EDIT-PROGRAM' }
    },
    {
      path: 'parent',
      component: ParentFormComponent,
      data: { title: 'Parent', breadcrumb: 'PARENT' }
    },
    {
      path: 'parent-update',
      component: ParentUpdateFormComponent,
      data: { title: 'Parent-Update', breadcrumb: 'PARENT-UPDATE' }
    },
    {
      path: 'add-category',
      component: AddcategoryFormComponent,
      data: { title: 'Add-Category', breadcrumb: 'ADD-CATEGORY' }
    },
    {
      path: 'category',
      component: CategoryFormComponent,
      data: { title: 'Category', breadcrumb: 'CATEGORY' }
    },
    {
      path: 'child/:id',
      component: ChildFormComponent,
      data: { title: 'Child', breadcrumb: 'CHILD' }
    },
    {
      path: 'provider',
      component: ProviderFormComponent,
      data: { title: 'Provider', breadcrumb: 'PROVIDER' }
    },
    {
      path: 'provider-update',
      component: ProviderUpdateFormComponent,
      data: { title: 'provider-update', breadcrumb: 'PROVIDER-UPDATE' }
    },
    {
      path: 'program',
      component: ProgramFormComponent,
      data: { title: 'Program', breadcrumb: 'PROGRAM' }
    },
    {
      path: 'admin',
      component: AdminFormComponent,
      data: { title: 'Admin', breadcrumb: 'ADMIN' }
    },]
  }
];
