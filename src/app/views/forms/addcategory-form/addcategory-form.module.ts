import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddcategoryFormRoutingModule } from './addcategory-form-routing.module';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    AddcategoryFormRoutingModule
  ]
})
export class AddcategoryFormModule { }
