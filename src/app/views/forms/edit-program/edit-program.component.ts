import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { FormBuilder, FormGroup, Validators, FormControl } from '@angular/forms';
import { ENTER, COMMA } from '@angular/cdk/keycodes';
import { Observable } from 'rxjs';
import { MatAutocomplete, MatAutocompleteSelectedEvent, MatChipInputEvent, MatSliderChange, MatDialogRef, MatDialog } from '@angular/material';
import { startWith, map } from 'rxjs/operators';
import { FileUploader } from 'ng2-file-upload';
import { ActivatedRoute, Router } from '@angular/router';
import { Userr } from 'app/shared/models/user.model';
import { Program } from 'app/shared/models/program.model';
import { Options } from 'ng5-slider';
import { ApiService } from 'app/shared/services/api.service.service';
import * as moment from 'moment';
import { AppLoaderService } from 'app/shared/services/app-loader/app-loader.service';
import { AddBatchPopupComponent } from '../wizard/add-batch-popup/add-batch-popup.component';
import { DataService } from 'app/shared/services/dataservice.service';

@Component({
  selector: 'app-edit-program',
  templateUrl: './edit-program.component.html',
  styleUrls: ['./edit-program.component.css']
})
export class EditProgramComponent implements OnInit {
  firstFormGroup: FormGroup;
  secondFormGroup: FormGroup;
  thirdFormGroup: FormGroup;
  fourthFormGroup: FormGroup;
  startDate = new Date;
  endDate = new Date;
  startTime = new Date;
  endTime = new Date;

  user = new Userr;
  program = new Program;

  //  ng5slider start age group

  minAge: number = 3;
  maxAge: number = 10;

  ageOption: Options = {
    floor: 0,
    ceil: 100,
    translate: (value: number): string => {
      return value + ' YRS';
    }

  };

  //  ng5slider start capacity

  minCapacity: number = 0;
  maxCapacity: number = 30;

  capacityOption: Options = {
    floor: 0,
    ceil: 100,
    translate: (value: number): string => {
      return value + '';
    }

  };
  // ng5slider end


  // ---------------autucomplete-------------  
  visible: boolean = true;
  selectable: boolean = true;
  removable: boolean = true;
  addOnBlur: boolean = false;
  keyword = 'name';

  separatorKeysCodes: number[] = [ENTER, COMMA];

  fruitCtrl = new FormControl();

  filteredFruits: Observable<any[]>;
  filteredValues: Observable<any[]>;

  tags: any = [];
  batches: any = [];
  // ng5slider end

  public uploader: FileUploader = new FileUploader({ url: 'upload_url' });
  public hasBaseDropZoneOver: boolean = false;



  constructor(private fb: FormBuilder,
    private apiservice: ApiService,
    private dialog: MatDialog,
    private loader: AppLoaderService,
    private activatedRoute: ActivatedRoute,
    private dataservice: DataService,
    private route: Router) {


    this.activatedRoute.params.subscribe(params => {
      this.user._id = params['id'];
    });
    this.program = this.dataservice.getOption();
    console.log('program data for edit ', this.program);

  }
  openPopUp() {
    let dialogRef: MatDialogRef<any> = this.dialog.open(AddBatchPopupComponent, {
      width: '30%',
      disableClose: true,
      // data: { title: title, payload: data }
    })
    dialogRef.afterClosed()
      .subscribe(res => {
        if (!res) {
          // If user press cancel
          return;
        }
        // this.loader.open();


      });
  }
  onFileChanged(e) { }

  onAgeChange(event: MatSliderChange) {
    console.log("This is emitted as the thumb slides");
    console.log(event.value);
  }

  remove(fruit, indx): void {
    this.program.tags.splice(indx, 1);
    console.log('tagsssss', this.program.tags);
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.program.tags.push(event.option.value);
  }



  selectEvent(item) {
    this.program.tags.push(item)
    console.log('selectEvent', item);
    console.log('intrests', this.program.tags);
  }

  onChangeSearch(key: string) {
    this.tags = []
    this.apiservice.searchTag(key).subscribe((res: any) => {
      this.tags = res;
      console.log('searchTag list categories', this.tags);
    });

    // fetch remote data from here
    // And reassign the 'data' which is binded to 'data' property.
  }

  onFocused(e) {
    console.log('onFocused', e)
    // do something when input is focused
  }

  ngOnInit() {
    this.firstFormGroup = new FormGroup({
      type: new FormControl(['',]),
    });
    this.secondFormGroup = new FormGroup({
      name: new FormControl(['',]),
      description: new FormControl(['',]),
      startDate: new FormControl(['',]),
      endDate: new FormControl(['',]),
      startTime: new FormControl(['',]),
      endTime: new FormControl(['',]),
      isFree: new FormControl(false),
      category: new FormControl(['',]),
      ageGroup: new FormControl(['',]),
      pricePerParticipant: new FormControl(['',]),
      priceForSiblings: new FormControl(['',]),



      // startDate: ['',],
      // endDate: ['',],
      // startTime: ['',],
      // endTime: ['',],
      // associates: ['',],
      // category: ['',],
      // ageGroup: [,],
      // imageUplad: [,],
      // isProgramFree: [true,],
      // pricePerParticipant: ['',],
      // priceForSiblings: ['',],




    });
    this.thirdFormGroup = new FormGroup({
      email: new FormControl(['', Validators.email]),
      specialInstructions: new FormControl(['',]),
      adultAssistanceIsRequried: new FormControl(false),
      addresses: new FormControl(['',]),
      days: new FormControl(['',]),
      hours: new FormControl(['',]),


      // email: ['',],
      // special_instruction: ['',],
      // isAdult_assistance: [true,],
      // booking_cancle_before: ['',],
      // address: ['',],

    });

  }

  // public fileOverBase(e: any): void {
  //   this.hasBaseDropZoneOver = e;
  // }


  updateProgram() {
    var batch: any;
    var response: any;
    // this.closePopup();
    // this.program.programCoverPic = this.getUrl;
    // this.program.userId = this.userData.id;
    let totalBatch = this.program.batches.length - 1
    // if (this.batchData) {
    //   for (let i = 0; i <= totalBatch; i++) {
    //     batch = this.program.batches[i];

    //     if (this.batchData._id === batch._id) {
    //       this.program.batches[i] = this.batchData;
    //       console.log('batchData in for loop', this.batchData);
    //       console.log('program.batches[i] in for loop', this.program.batches[i]);
    //     }
    //   }
    // }
    console.log('program data before upload', this.program);
    this.loader.open();
    this.apiservice.updateProgram(this.program._id, this.program).subscribe(res => {
      response = res;
      this.loader.close();
      console.log('updated program', response);
      if (response.isSuccess === true) {
        this.route.navigate(['tables/program', this.user._id]);
      } else {
        // this.snack.open(this.updateProgramResponse.error, 'OK', { duration: 5000 });
      }
    });
  }

  submit() {
    console.log(this.firstFormGroup.value);
    console.log(this.secondFormGroup.value);
    console.log(this.thirdFormGroup.value);

    console.log('add program data before upload ', this.program);
    this.updateProgram();


  }
}
