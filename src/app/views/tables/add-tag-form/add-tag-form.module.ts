import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { AddTagFormRoutingModule } from './add-tag-form-routing.module';
import { ReactiveFormsModule } from '@angular/forms';


@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ReactiveFormsModule,
    AddTagFormRoutingModule
  ]
})
export class AddTagFormModule { }
