import { Component, OnInit } from '@angular/core';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { Router, ActivatedRoute } from '@angular/router';
import { DataService } from 'app/shared/services/dataservice.service';
import { ApiService } from 'app/shared/services/api.service.service';
import { FileUploader } from 'ng2-file-upload';
import { Userr } from 'app/shared/models/user.model';
import { AppConfirmService } from 'app/shared/services/app-confirm/app-confirm.service';
import { AppLoaderService } from 'app/shared/services/app-loader/app-loader.service';
import { MatSnackBar, MatSnackBarConfig, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition } from '@angular/material';

@Component({
  selector: 'app-program-table',
  templateUrl: './program-table.component.html',
  styleUrls: ['./program-table.component.scss']
})
export class ProgramTableComponent implements OnInit {
  isLoading: boolean;
  usersData: any = {};
  rows: any = [];
  pageNo: number = 1;
  pageSize: number;
  temp: any = [];
  ColumnMode = ColumnMode;
  user = new Userr;

  submitted: any;
  isShow = true;
  public uploader: FileUploader = new FileUploader({ url: 'upload_url' });
  public hasBaseDropZoneOver: boolean = false;



  message: string = 'Program Deleted Successfully!';
  // actionButtonLabel: string = 'Retry';
  action: boolean = true;
  setAutoHide: boolean = true;
  autoHide: number = 4000;
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'bottom';
  constructor(
    public route: Router,
    private dataservice: DataService,
    private apiservice: ApiService,
    private snack: MatSnackBar,
    private activatedRoute: ActivatedRoute,
    private confirmService: AppConfirmService,
    private loader: AppLoaderService,
  ) {
    this.activatedRoute.params.subscribe(params => {
      this.user._id = params['id'];
    });
    let config = new MatSnackBarConfig();
    config.verticalPosition = this.verticalPosition;
    config.horizontalPosition = this.horizontalPosition;
    config.duration = this.setAutoHide ? this.autoHide : 0;
  }

  // edit(data) {
  //   this.dataservice.setOption(data);
  //   // this.route.navigate(['/requests/fill', JSON.stringify(data)]);
  //   this.route.navigate(['forms/basic']);

  // }
  //   activeSkill(event){
  //     event.target.setAttribute('color', 'accent');
  // }

  // ngOnChanges() {
  //   this.columns = this.service.getDataConf();
  //   this.rows = this.service.getAll();

  // }


  edit(data) {
    this.dataservice.setOption(data);
    this.route.navigate(['forms/edit-program', this.user._id]);
    // this.route.navigate(['forms/program']);
  }
  back() {

    this.route.navigate(['tables/provider']);
  }
  showHideButton() {
    this.isShow = !this.isShow;
  }
  add() {
    this.route.navigate(['forms/wizard', this.user._id]);
  }
  getProgram() {
    if (this.user._id === 'id') {
      this.loader.open();
      this.isLoading = true;
      this.pageSize = 1000;
      this.apiservice.getProgram(this.pageNo, this.pageSize).subscribe(res => {
        this.temp = res;
        this.rows = this.temp;
        this.loader.close();
        console.log('all programs list =>>', this.rows);
        this.isLoading = false;
      });
    } else {
      this.isLoading = true;
      this.pageSize = 1000;
      this.loader.open();
      this.apiservice.getProgramByProvider(this.user._id, this.pageNo, this.pageSize).subscribe(res => {
        this.temp = res;
        this.rows = this.temp;
        this.loader.close();
        console.log('programs list by provider =>>', this.rows);
        this.isLoading = false;
      });
    }
  }

  programActiveInActive(program) {
    let programStatus = '';
    if (program.status === 'active') {
      this.loader.open();
      programStatus = 'inactive';
      this.apiservice.programActiveInActive(program._id, programStatus).subscribe(res => {
        this.loader.close();
        this.getProgram();
      });

    }
    else {
      this.loader.open();
      programStatus = 'active';
      this.apiservice.programActiveInActive(program._id, programStatus).subscribe(res => {
        this.loader.close();
        this.getProgram();
      });

    }
  }

  deleteProgram(data) {
    this.confirmService.confirm({ message: `Delete ${data.name}?` }).subscribe(res => {
      if (res) {
        this.loader.open();
        this.isLoading = true;
        this.apiservice.deleteProgram(data._id).subscribe(res => {
          var response: any = res;
          console.log('', response)
          if (response.isSuccess === true) {
            this.loader.close();
            this.snack.open(this.message, 'OK', { duration: 4000 });
            this.route.navigateByUrl('/tables', { skipLocationChange: true }).then(() => {
              this.route.navigate(['tables/program', this.user._id]);
            })

          } else {
            this.loader.close();
            let msg = "Something Went Wrong!";
            this.snack.open(msg, 'OK', { duration: 4000 });
          }
        })
      }
    })
  }

  ngOnInit() {
    this.rows;
    this.getProgram();

  }
  public fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }
  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    var columns = Object.keys(this.temp[0]);
    // Removes last "$$index" from "column"
    columns.splice(columns.length - 1);

    // console.log(columns);
    if (!columns.length)
      return;

    const rows = this.temp.filter(function (d) {
      for (let i = 0; i <= columns.length; i++) {
        let column = columns[i];
        // console.log(d[column]);
        if (d[column] && d[column].toString().toLowerCase().indexOf(val) > -1) {
          return true;
        }
      }
    });

    this.rows = rows;

  }

}