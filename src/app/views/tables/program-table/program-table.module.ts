import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProgramTableRoutingModule } from './program-table-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ProgramTableRoutingModule
  ]
})
export class ProgramTableModule { }
