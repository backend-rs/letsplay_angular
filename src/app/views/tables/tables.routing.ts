import { Routes } from '@angular/router';

import { FullscreenTableComponent } from './fullscreen-table/fullscreen-table.component';
import { PagingTableComponent } from './paging-table/paging-table.component';
import { FilterTableComponent } from './filter-table/filter-table.component';
import { MaterialTableComponent } from './material-table/material-table.component';
import { ProviderTableComponent } from './provider-table/provider-table.component';
import { CategoryTableComponent } from './category-table/category-table.component';
import { ReoprtsComponent } from './reoprts/reoprts.component'
import { TagTableComponent } from './tag-table/tag-table.component';
import { ChildReportsComponent } from './child-reports/child-reports.component';
import { ProgramsReportsComponent } from './programs-reports/programs-reports.component';
import { ParentReportsComponent } from './parent-reports/parent-reports.component';
import { TagEditTableComponent } from './tag-edit-table/tag-edit-table.component';
import { AdminTableComponent } from './admin-table/admin-table.component';
import { ProgramTableComponent } from './program-table/program-table.component';
import { AddTagFormComponent } from './add-tag-form/add-tag-form.component';
import { ClaimsTableComponent } from './claims-table/claims-table.component';

export const TablesRoutes: Routes = [
  {
    path: '',
    children: [{
      path: 'fullscreen',
      component: FullscreenTableComponent,
      data: { title: 'Fullscreen', breadcrumb: 'FULLSCREEN' }
    }, {
      path: 'paging',
      component: PagingTableComponent,
      data: { title: '', breadcrumb: 'PAGING' }
    },
    {
      path: 'admin',
      component: AdminTableComponent,
      data: { title: 'Admin', breadcrumb: 'ADMIN' }
    },

    {
      path: 'category',
      component: CategoryTableComponent,
      data: { title: '', breadcrumb: 'PAGING' }
    },
    {
      path: 'provider',
      component: ProviderTableComponent,
      data: { title: '', breadcrumb: 'PAGING' }
    }, {
      path: 'filter',
      component: FilterTableComponent,
      data: { title: 'Filter', breadcrumb: 'FILTER' }
    },
    {
      path: 'mat-table',
      component: MaterialTableComponent,
      data: { title: 'Material TAble', breadcrumb: 'Material Table' }
    },
    {
      path: 'tag',
      component: TagTableComponent,
      data: { title: 'Tag', breadcrumb: 'TAG' }
    },
    {
      path: 'edit-tag',
      component: TagEditTableComponent,
      data: { title: 'edit-tag', breadcrumb: 'edit-tag' }
    },
    {
      path: 'reports',
      component: ReoprtsComponent,
      // data: { title: 'Material TAble', breadcrumb: 'Material Table' }
    },
    {
      path: 'childReports',
      component: ChildReportsComponent,
      // data: { title: 'Material TAble', breadcrumb: 'Material Table' }
    },
    {
      path: 'parentReports',
      component: ParentReportsComponent,
      // data: { title: 'Material TAble', breadcrumb: 'Material Table' }
    }, {
      path: 'programReports',
      component: ProgramsReportsComponent,
      // data: { title: 'Material TAble', breadcrumb: 'Material Table' }
    },
    {
      path: 'program/:id',
      component: ProgramTableComponent,
      data: { title: 'Program', breadcrumb: 'PROGRAM' }
    },
    {
      path: 'add-tag',
      component: AddTagFormComponent,
      data: { title: 'add-tag', breadcrumb: 'add-tag' }
    },
    {
      path: 'claims',
      component: ClaimsTableComponent,
      data: { title: 'claims', breadcrumb: 'claims' }
    }
    ]
  }
];
