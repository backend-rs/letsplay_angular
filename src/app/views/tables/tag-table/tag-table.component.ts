import { Component, OnInit } from '@angular/core';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { Router } from '@angular/router';
import { DataService } from 'app/shared/services/dataservice.service';
import { ApiService } from 'app/shared/services/api.service.service';
import { AppLoaderService } from 'app/shared/services/app-loader/app-loader.service';

@Component({
  selector: 'app-tag-table',
  templateUrl: './tag-table.component.html',
  styleUrls: ['./tag-table.component.scss']
})
export class TagTableComponent implements OnInit {
  rows = [];
  ColumnMode = ColumnMode;
  tag: any;



  temp = []

  constructor(private route: Router,
    private loader: AppLoaderService,
    private dataservice: DataService,
    private apiservice: ApiService) { }
  edit(data) {
    this.dataservice.setOption(data);

    this.route.navigate(['tables/edit-tag']);
  }
  add() {

    this.route.navigate(['tables/add-tag']);
  }
  getTag() {
    this.loader.open();
    this.apiservice.getTag().subscribe(res => {
      this.temp = res;
      this.rows = this.temp;
      this.loader.close();
      console.log('resssssssssss', this.temp);
      // this.temp.sort = this.sort; 
    });
  }

  ngOnInit() {
    this.getTag();
    this.rows;

  }
  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    var columns = Object.keys(this.temp[0]);
    // Removes last "$$index" from "column"
    columns.splice(columns.length - 1);

    // console.log(columns);
    if (!columns.length)
      return;

    const rows = this.temp.filter(function (d) {
      for (let i = 0; i <= columns.length; i++) {
        let column = columns[i];
        // console.log(d[column]);
        if (d[column] && d[column].toString().toLowerCase().indexOf(val) > -1) {
          return true;
        }
      }
    });

    this.rows = rows;

  }

}



