import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TagTableRoutingModule } from './tag-table-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    TagTableRoutingModule
  ]
})
export class TagTableModule { }
