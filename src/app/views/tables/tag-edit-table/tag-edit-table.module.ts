import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { TagEditTableRoutingModule } from './tag-edit-table-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    TagEditTableRoutingModule
  ]
})
export class TagEditTableModule { }
