
import { COMMA, ENTER, SEMICOLON } from '@angular/cdk/keycodes';
import { Component } from '@angular/core';
import { MatChipInputEvent, MatSnackBar, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition, MatSnackBarConfig, MatAutocompleteSelectedEvent } from '@angular/material';
import { Router } from '@angular/router';
import { DataService } from 'app/shared/services/dataservice.service';
import { ApiService } from 'app/shared/services/api.service.service';
import { FormGroup, FormControl, Validators } from '@angular/forms';
import { Observable } from 'rxjs';
import { startWith, map } from 'rxjs/operators';
import { AppLoaderService } from 'app/shared/services/app-loader/app-loader.service';

export interface Fruit {
  name: string;
}

@Component({
  selector: 'app-tag-edit-table',
  templateUrl: './tag-edit-table.component.html',
  styleUrls: ['./tag-edit-table.component.scss']
})
export class TagEditTableComponent {
  submitted = false;
  updatetagForm: FormGroup;
  visible: boolean = true;
  selectable = "selectable"
  removable = true;
  addOnBlur = true;
  separatorKeysCodes: number[] = [ENTER, COMMA, SEMICOLON];
  categoryCtrl = new FormControl();

  filteredCategories: Observable<any[]>;


  allCategories = [];

  tag: any = [];

  message: string = 'Tag Updated !';
  action: boolean = true;
  setAutoHide: boolean = true;
  autoHide: number = 4000;
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'bottom';
  categoryInput: any;
  tagResponse: any;

  // addExtraClass: boolean = false;
  constructor(private route: Router, private dataservice: DataService,
    private apiservice: ApiService,
    private snack: MatSnackBar,
    private loader: AppLoaderService
  ) {
    this.tag = dataservice.getOption();
    this.filteredCategories = this.categoryCtrl.valueChanges.pipe(
      startWith(null),
      map((category: string | null) => category ? this.filter(category) : this.allCategories.slice()));

    let config = new MatSnackBarConfig();
    config.verticalPosition = this.verticalPosition;
    config.horizontalPosition = this.horizontalPosition;
    config.duration = this.setAutoHide ? this.autoHide : 0;
  }

  add(event: MatChipInputEvent): void {
    const input = event.input;
    const value = event.value;

    // Add our fruit
    if ((value || '').trim()) {
      this.tag.categoryIds.push(value.trim());
    }

    // Reset the input value
    if (input) {
      input.value = '';
    }

    this.categoryCtrl.setValue(null);
    console.log('add id', this.tag);
  }

  remove(category: any): void {
    const index = this.tag.categoryIds.indexOf(category);

    if (index >= 0) {
      this.tag.categoryIds.splice(index, 1);
    }
  }

  filter(name: string) {
    return this.allCategories.filter(category =>
      category.name.toLowerCase().indexOf(name.toLowerCase()) === 0);
  }

  selected(event: MatAutocompleteSelectedEvent): void {
    this.tag.categoryIds.push(event.option.value);
    this.categoryInput.nativeElement.value = '';
    this.categoryCtrl.setValue(null);
  }

  back() {
    this.route.navigate(['tables/tag']);

  }
  updateTag() {
    this.loader.open();
    this.apiservice.updateTag(this.tag._id, this.tag).subscribe(res => {
      console.log('resssupdatetag', res);
      this.tagResponse = res;
      this.loader.close(); if (this.tagResponse.isSuccess === true) {
        this.snack.open(this.message, 'OK', { duration: 4000 });
        this.route.navigate(['tables/tag']);

      } else {
        let msg = "Something Went Wrong!";
        this.snack.open(msg, 'OK', { duration: 4000 });
      }
    });


  }
  ngOnInit() {
    this.apiservice.getCategory().subscribe(res => {
      this.allCategories = res;
      console.log('category list', this.allCategories);
    });
    this.updatetagForm = new FormGroup({
      name: new FormControl('', Validators.required),
      description: new FormControl('', Validators.required),


    });
  }

  onSubmit() {

    this.submitted = true;
    return this.updateTag();
  }

}

