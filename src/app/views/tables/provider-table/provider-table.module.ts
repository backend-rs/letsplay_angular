import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ProviderTableRoutingModule } from './provider-table-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ProviderTableRoutingModule
  ]
})
export class ProviderTableModule { }
