import { Component, OnInit } from '@angular/core';
import { ApiService } from 'app/shared/services/api.service.service';
import { DataService } from 'app/shared/services/dataservice.service';
import { Router } from '@angular/router';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { FileUploader } from 'ng2-file-upload';
import { AppConfirmService } from 'app/shared/services/app-confirm/app-confirm.service';
import { AppLoaderService } from 'app/shared/services/app-loader/app-loader.service';
import { MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition, MatSnackBar, MatSnackBarConfig } from '@angular/material';

@Component({
  selector: 'app-provider-table',
  templateUrl: './provider-table.component.html',
  styleUrls: ['./provider-table.component.scss']
})
export class ProviderTableComponent implements OnInit {
  isLoading: boolean;
  rows: any = [];
  temp: any = [];
  provider = "provider";
  ColumnMode = ColumnMode;
  isShow = true;

  public uploader: FileUploader = new FileUploader({ url: 'upload_url' });
  public hasBaseDropZoneOver: boolean = false;



  message: string = 'Provider Deleted Successfully!';
  // actionButtonLabel: string = 'Retry';
  action: boolean = true;
  setAutoHide: boolean = true;
  autoHide: number = 4000;
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'bottom';

  constructor(private apiservice: ApiService,
    private dataservice: DataService,
    private route: Router,
    private confirmService: AppConfirmService,
    private loader: AppLoaderService,
    private snack: MatSnackBar,
  ) {
    let config = new MatSnackBarConfig();
    config.verticalPosition = this.verticalPosition;
    config.horizontalPosition = this.horizontalPosition;
    config.duration = this.setAutoHide ? this.autoHide : 0;
  }
  getProviders() {
    this.loader.open();
    this.apiservice.getUsers(this.provider).subscribe(res => {
      this.temp = res;
      this.rows = this.temp;
      this.loader.close();
      console.log('providers', this.rows)
    });
  }
  providerProfile(data) {
    // this.dataservice.setOption(data);
    this.route.navigate(['tables/program', data.id]);

  }
  edit(data) {
    this.dataservice.setOption(data);
    // this.route.navigate(['/requests/fill', JSON.stringify(data)]);
    this.route.navigate(['forms/provider-update']);
  }
  add() {
    this.route.navigate(['forms/provider']);
  }
  showHideButton() {
    this.isShow = !this.isShow;
  }
  activeSkill(event) {
    event.target.setAttribute('color', 'accent');
  }
  deleteProvider(data) {
    this.confirmService.confirm({ message: `Delete ${data.firstName}?` }).subscribe(res => {
      if (res) {
        this.loader.open();
        this.isLoading = true;
        this.apiservice.deleteUser(data.id).subscribe(res => {
          var response: any = res;
          console.log('', response)
          if (response.isSuccess === true) {
            this.loader.close();
            // this.snack.open('Member deleted!', 'OK', { duration: 4000 })
            this.snack.open(this.message, 'OK', { duration: 4000 });
            this.route.navigateByUrl('/tables', { skipLocationChange: true }).then(() => {
              this.route.navigate(['tables/provider']);
            })

          } else {
            this.loader.close();
            let msg = "Something Went Wrong!";
            this.snack.open(msg, 'OK', { duration: 4000 });
          }
        })
      }
    })
  }
  providerActiveInActive(provider) {
    let isActivated: boolean;
    if (provider.isActivated === true) {
      isActivated = false;
      this.loader.open();
      this.apiservice.userActiveInActive(provider.id, isActivated).subscribe(res => {
        this.loader.close();
        this.getProviders();

      });

    }
    else {
      this.loader.open();
      isActivated = true;
      this.apiservice.userActiveInActive(provider.id, isActivated).subscribe(res => {
        this.loader.close();
        this.getProviders();
      });

    }
  }
  ngOnInit() {
    this.rows;
    this.getProviders();

    // this.columns = this.service.getDataConf();

  }
  public fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }

  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    var columns = Object.keys(this.temp[0]);
    // Removes last "$$index" from "column"
    columns.splice(columns.length - 1);

    // console.log(columns);
    if (!columns.length)
      return;

    const rows = this.temp.filter(function (d) {
      for (let i = 0; i <= columns.length; i++) {
        let column = columns[i];
        // console.log(d[column]);
        if (d[column] && d[column].toString().toLowerCase().indexOf(val) > -1) {
          return true;
        }
      }
    });

    this.rows = rows;

  }

}
