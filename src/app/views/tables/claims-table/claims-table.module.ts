import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { ClaimsTableRoutingModule } from './claims-table-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    ClaimsTableRoutingModule
  ]
})
export class ClaimsTableModule { }
