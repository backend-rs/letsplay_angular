import { Component, OnInit, OnChanges, ViewChild } from '@angular/core';
import { TablesService } from '../tables.service';
import { Router } from '@angular/router';
import { DataService } from 'app/shared/services/dataservice.service';
import { ApiService } from 'app/shared/services/api.service.service';
import { MatSort, MatSnackBarConfig, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition, MatSnackBar } from '@angular/material';
import { SortType, ColumnMode } from '@swimlane/ngx-datatable';
import { FileUploader } from 'ng2-file-upload';
import { AppConfirmService } from '../../../shared/services/app-confirm/app-confirm.service';
import { AppLoaderService } from '../../../shared/services/app-loader/app-loader.service';
import { Claim } from 'app/shared/models/claim.model';
@Component({
  selector: 'app-paging-table',
  templateUrl: './claims-table.component.html',
  styleUrls: ['./claims-table.component.css'],
  providers: [TablesService]
})
export class ClaimsTableComponent implements OnInit, OnChanges {
  // @ViewChild(MatSort, {static: false}) sort: MatSort;
  // columns:any = [];

  isLoading: boolean;
  usersData: any = {};
  rows: any = [];
  temp: any = [];
  ColumnMode = ColumnMode;
  submitted: any;
  claim = new Claim;

  public uploader: FileUploader = new FileUploader({ url: 'upload_url' });
  public hasBaseDropZoneOver: boolean = false;
  parentResponse: any;


  message: string = 'Parent Deleted Successfully!';
  // actionButtonLabel: string = 'Retry';
  action: boolean = true;
  setAutoHide: boolean = true;
  autoHide: number = 4000;
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'bottom';
  categoryResponse: any;

  // addExtraClass: boolean = false;



  constructor(private service: TablesService,
    public route: Router,
    private dataservice: DataService,
    private snack: MatSnackBar,
    private apiservice: ApiService,
    private confirmService: AppConfirmService,
    private loader: AppLoaderService
  ) {

    let config = new MatSnackBarConfig();
    config.verticalPosition = this.verticalPosition;
    config.horizontalPosition = this.horizontalPosition;
    config.duration = this.setAutoHide ? this.autoHide : 0;
    // config.extraClasses = this.addExtraClass ? ['test'] : undefined;
  }

  getClaimsList() {
    this.loader.open();
    this.apiservice.getClaimsList().subscribe(res => {
      console.log('claims list', res)
      this.temp = res;
      this.rows = this.temp;
      this.loader.close();
    });
  }
  claimActionToApprove(data) {
    this.claim._id = data._id;
    this.claim.status = data.status = "approve";
    this.claim.requestBy = data.requestBy;
    this.claim.requestOn = data.requestOn._id;
    this.loader.open();
    this.apiservice.claimAction(this.claim).subscribe(res => {
      console.log('claims list', res)
      this.loader.close();
      this.getClaimsList();
    });

  }
  claimActionToReject(data) {
    this.claim._id = data._id;
    this.claim.status = data.status = "reject";
    this.claim.requestBy = data.requestBy;
    this.claim.requestOn = data.requestOn._id;
    this.loader.open();
    this.apiservice.claimAction(this.claim).subscribe(res => {
      console.log('claims list', res)
      this.loader.close();
      this.getClaimsList();
    });

  }

  ngOnChanges() {

  }
  ngOnInit() {
    this.rows;
    this.getClaimsList();

  }

  public fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }
  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    var columns = Object.keys(this.temp[0]);
    // Removes last "$$index" from "column"
    columns.splice(columns.length - 1);

    // console.log(columns);
    if (!columns.length)
      return;

    const rows = this.temp.filter(function (d) {
      for (let i = 0; i <= columns.length; i++) {
        let column = columns[i];
        // console.log(d[column]);
        if (d[column] && d[column].toString().toLowerCase().indexOf(val) > -1) {
          return true;
        }
      }
    });

    this.rows = rows;

  }

}
