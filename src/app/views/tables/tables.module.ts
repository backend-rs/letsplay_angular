import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import {
  MatInputModule, MatPaginatorModule, MatDatepickerModule, MatIconModule, MatTableDataSource, MatTableModule, MatSortModule, MatButtonModule, MatChip, MatCardModule, MatMenuModule, MatChipsModule, MatTooltipModule, MatListModule, MatDialogModule, MatSnackBarModule, MatSlideToggleModule, MatGridListModule, MatCheckboxModule, MatRadioModule, MatProgressBarModule, MatTabsModule, MatOptionModule, MatAutocompleteModule
} from '@angular/material';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { FilterTableComponent } from './filter-table/filter-table.component';
import { FullscreenTableComponent } from './fullscreen-table/fullscreen-table.component';
import { PagingTableComponent } from './paging-table/paging-table.component';
import { TablesRoutes } from './tables.routing';
import { MaterialTableComponent } from './material-table/material-table.component';
import { ProviderTableComponent } from './provider-table/provider-table.component';
import { CategoryTableComponent } from './category-table/category-table.component';
import { ReoprtsComponent } from './reoprts/reoprts.component';
import { FlexLayoutModule } from '@angular/flex-layout';
import { ChartsModule } from 'ng2-charts';
import { FileUploadModule } from 'ng2-file-upload';
import { SharedPipesModule } from 'app/shared/pipes/shared-pipes.module';
import { TagTableComponent } from './tag-table/tag-table.component';
import { ChildReportsComponent } from './child-reports/child-reports.component';
import { ProgramsReportsComponent } from './programs-reports/programs-reports.component';
import { ParentReportsComponent } from './parent-reports/parent-reports.component';
import { TagEditTableComponent } from './tag-edit-table/tag-edit-table.component';
import { CrudService } from '../cruds/crud.service';
import { AdminTableComponent } from './admin-table/admin-table.component';
import { ProgramTableComponent } from './program-table/program-table.component';
import { AddTagFormComponent } from './add-tag-form/add-tag-form.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { ClaimsTableComponent } from './claims-table/claims-table.component';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    ReactiveFormsModule,
    MatIconModule,
    MatButtonModule,
    MatInputModule,
    MatPaginatorModule,
    MatSortModule,
    MatTableModule,
    MatInputModule,
    MatIconModule,
    MatCardModule,
    MatMenuModule,
    MatButtonModule,
    MatChipsModule,
    MatListModule,
    MatTooltipModule,
    MatDialogModule,
    MatDatepickerModule,
    MatSnackBarModule,
    MatSlideToggleModule,
    NgxDatatableModule,
    MatListModule,
    MatIconModule,
    MatButtonModule,
    MatCardModule,
    MatMenuModule,
    MatSlideToggleModule,
    MatGridListModule,
    MatCheckboxModule,
    MatRadioModule,
    MatTabsModule,
    MatProgressBarModule,
    FlexLayoutModule,
    ChartsModule,
    MatOptionModule,
    MatAutocompleteModule,
    MatSnackBarModule,
    FileUploadModule,
    SharedPipesModule,
    RouterModule.forChild(TablesRoutes)
  ],

  declarations: [FilterTableComponent,
    FullscreenTableComponent,
    PagingTableComponent,
    MaterialTableComponent,
    ProviderTableComponent,
    CategoryTableComponent,
    AddTagFormComponent,
    ReoprtsComponent,
    TagTableComponent,
    TagEditTableComponent,
    ChildReportsComponent,
    ProgramsReportsComponent,
    ParentReportsComponent,
    AdminTableComponent,
    ProgramTableComponent,
    ClaimsTableComponent],
})
export class TablesModule { }
