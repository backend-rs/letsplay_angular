import { Component, OnInit } from '@angular/core';
import { ColumnMode } from '@swimlane/ngx-datatable';
import { Router } from '@angular/router';
import { DataService } from 'app/shared/services/dataservice.service';
import { ApiService } from 'app/shared/services/api.service.service';
import { AppLoaderService } from '../../../shared/services/app-loader/app-loader.service';

@Component({
  selector: 'app-category-table',
  templateUrl: './category-table.component.html',
  styleUrls: ['./category-table.component.scss']
})
export class CategoryTableComponent implements OnInit {
  rows = [];
  ColumnMode = ColumnMode;


  temp = [];

  constructor(private route: Router, private dataservice: DataService, private loader: AppLoaderService,
    private apiservice: ApiService) { }
  edit(data) {
    this.dataservice.setOption(data);

    this.route.navigate(['forms/category']);
  }
  add() {
    this.route.navigate(['forms/add-category']);
  }
  getCategory() {
    this.loader.open();
    this.apiservice.getCategory().subscribe(res => {
      this.rows = res;
      this.loader.close();
      console.log('rows', this.rows);
    });
  }

  ngOnInit() {
    this.getCategory();
    this.rows;
  }
  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    if (this.rows.length > 1) {
      var columns = Object.keys(this.rows[0]);
      columns.splice(columns.length - 1);
      if (!columns.length)
        return;

      const rows = this.temp.filter(function (d) {
        for (let i = 0; i <= columns.length; i++) {
          let column = columns[i];
          // console.log(d[column]);
          if (d[column] && d[column].toString().toLowerCase().indexOf(val) > -1) {
            return true;
          }
        }
      });
      this.rows = rows;
    }

  }

}
