import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';

import { PagingTableRoutingModule } from './paging-table-routing.module';

@NgModule({
  declarations: [],
  imports: [
    CommonModule,
    PagingTableRoutingModule
  ]
})
export class PagingTableModule { }
