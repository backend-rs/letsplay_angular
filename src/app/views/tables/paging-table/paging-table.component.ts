import { Component, OnInit, OnChanges, ViewChild } from '@angular/core';
import { TablesService } from '../tables.service';
import { Router } from '@angular/router';
import { DataService } from 'app/shared/services/dataservice.service';
import { ApiService } from 'app/shared/services/api.service.service';
import { MatSort, MatSnackBarConfig, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition, MatSnackBar } from '@angular/material';
import { SortType, ColumnMode } from '@swimlane/ngx-datatable';
import { FileUploader } from 'ng2-file-upload';
import { AppConfirmService } from '../../../shared/services/app-confirm/app-confirm.service';
import { AppLoaderService } from '../../../shared/services/app-loader/app-loader.service';
import { Userr } from 'app/shared/models/user.model';
@Component({
  selector: 'app-paging-table',
  templateUrl: './paging-table.component.html',
  styleUrls: ['./paging-table.component.css'],
  providers: [TablesService]
})
export class PagingTableComponent implements OnInit, OnChanges {
  // @ViewChild(MatSort, {static: false}) sort: MatSort;
  // columns:any = [];

  isLoading: boolean;
  usersData: any = {};
  rows: any = [];
  temp: any = [];
  ColumnMode = ColumnMode;

  pageSize: number;
  pageNo = 1;
  submitted: any;
  // isShow=true;

  public uploader: FileUploader = new FileUploader({ url: 'upload_url' });
  public hasBaseDropZoneOver: boolean = false;
  parentResponse: any;


  message: string = 'Parent Deleted Successfully!';
  // actionButtonLabel: string = 'Retry';
  action: boolean = true;
  setAutoHide: boolean = true;
  autoHide: number = 4000;
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'bottom';
  categoryResponse: any;

  // addExtraClass: boolean = false;



  constructor(private service: TablesService,
    public route: Router,
    private dataservice: DataService,
    private snack: MatSnackBar,
    private apiservice: ApiService,
    private confirmService: AppConfirmService,
    private loader: AppLoaderService
  ) {

    let config = new MatSnackBarConfig();
    config.verticalPosition = this.verticalPosition;
    config.horizontalPosition = this.horizontalPosition;
    config.duration = this.setAutoHide ? this.autoHide : 0;
    // config.extraClasses = this.addExtraClass ? ['test'] : undefined;
  }

  edit(data) {
    this.dataservice.setOption(data);
    // this.route.navigate(['/requests/fill', JSON.stringify(data)]);
    this.route.navigate(['forms/parent-update']);

  }
  activeSkill(event) {
    event.target.setAttribute('color', 'accent');
  }
  parentProfile(data) {
    this.dataservice.setOption(data);
    this.route.navigate(['profile/child']);

  }
  add() {
    this.route.navigate(['forms/parent']);
  }

  getParents() {
    this.isLoading = true;
    this.loader.open();
    this.apiservice.getParents().subscribe(res => {
      this.temp = res;
      this.rows = this.temp;
      this.loader.close();

      this.isLoading = false;
    });
  }
  deleteParent(data) {
    this.confirmService.confirm({ message: `Delete ${data.firstName}?` }).subscribe(res => {
      if (res) {
        this.loader.open();
        this.isLoading = true;
        this.apiservice.deleteUser(data._id).subscribe(res => {
          this.parentResponse = res;
          console.log('', this.parentResponse)
          if (this.parentResponse.isSuccess === true) {
            this.loader.close();
            // this.snack.open('Member deleted!', 'OK', { duration: 4000 })
            this.snack.open(this.message, 'OK', { duration: 4000 });
            this.route.navigateByUrl('/tables', { skipLocationChange: true }).then(() => {
              this.route.navigate(['tables/paging']);
            })

          } else {
            this.loader.close();
            let msg = "Something Went Wrong!";
            this.snack.open(msg, 'OK', { duration: 4000 });
          }
        })
      }
    })
  }
  parentActiveInActive(parent) {
    let isActivated: boolean;
    if (parent.isActivated === true) {
      this.loader.open();
      isActivated = false;
      this.apiservice.userActiveInActive(parent._id, isActivated).subscribe(res => {
        this.loader.close();
        this.getParents();

      });

    }
    else {
      this.loader.open();
      isActivated = true;
      this.apiservice.userActiveInActive(parent._id, isActivated).subscribe(res => {
        this.loader.close();
        this.getParents();

      });

    }
  }
  ngOnChanges() {
    // this.columns = this.service.getDataConf();
    // this.rows = this.service.getAll();

  }
  ngOnInit() {

    this.rows;
    this.getParents();

    // this.columns = this.service.getDataConf();

  }

  public fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }
  updateFilter(event) {
    const val = event.target.value.toLowerCase();
    var columns = Object.keys(this.temp[0]);
    // Removes last "$$index" from "column"
    columns.splice(columns.length - 1);

    // console.log(columns);
    if (!columns.length)
      return;

    const rows = this.temp.filter(function (d) {
      for (let i = 0; i <= columns.length; i++) {
        let column = columns[i];
        // console.log(d[column]);
        if (d[column] && d[column].toString().toLowerCase().indexOf(val) > -1) {
          return true;
        }
      }
    });

    this.rows = rows;

  }

}
