import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from "@angular/router";
import { DataService } from 'app/shared/services/dataservice.service';
import { isThisQuarter } from 'date-fns';
import { ApiService } from 'app/shared/services/api.service.service';
import { User } from '../app-chats/chat.service';
import { Userr } from 'app/shared/models/user.model';
import { AppLoaderService } from 'app/shared/services/app-loader/app-loader.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html'
})
export class ProfileComponent implements OnInit {

  activeView: string = 'overview';

  // Doughnut
  doughnutChartColors: any[] = [{
    backgroundColor: ['#fff', 'rgba(0, 0, 0, .24)',]
  }];

  total1: number = 500;
  data1: number = 200;
  doughnutChartData1: number[] = [this.data1, (this.total1 - this.data1)];

  total2: number = 1000;
  data2: number = 400;
  doughnutChartData2: number[] = [this.data2, (this.total2 - this.data2)];

  doughnutChartType = 'doughnut';
  doughnutOptions: any = {
    cutoutPercentage: 85,
    responsive: true,
    maintainAspectRatio: true,
    legend: {
      display: false,
      position: 'bottom'
    },
    elements: {
      arc: {
        borderWidth: 0,
      }
    },
    tooltips: {
      enabled: false
    }
  };
  user: any = new Userr;

  constructor(private router: ActivatedRoute, private route: Router, private dataservice: DataService,
    private apiservice: ApiService,
    private loader: AppLoaderService) {
    this.user = dataservice.getOption();
    console.log('parent data from parent table = > ', this.user);
  }
  resetPasswordLink(data) {
    this.route.navigate(['profile/settings']);
  }
  getUserById() {
    this.apiservice.getUserById(this.user._id).subscribe(res => {
      this.user = res;
      console.log('res', res);
      // this.user = res;

    });
  }

  ngOnInit() {
    this.activeView = this.router.snapshot.params['view']
    this.getUserById();
    console.log('userrrrrrr', this.user);
  }

}
