import { Component, OnInit } from '@angular/core';
import { FileUploader, FileItem } from 'ng2-file-upload';
import { DataService } from 'app/shared/services/dataservice.service';
import { Router } from '@angular/router';
import { ApiService } from 'app/shared/services/api.service.service';
import { MatSnackBarConfig, MatSnackBarHorizontalPosition, MatSnackBarVerticalPosition, MatSnackBar } from '@angular/material';
import { AppLoaderService } from 'app/shared/services/app-loader/app-loader.service';

@Component({
  selector: 'app-profile-settings',
  templateUrl: './profile-settings.component.html',
  styleUrls: ['./profile-settings.component.css']
})
export class ProfileSettingsComponent implements OnInit {
  //  uploader: FileUploader = new FileUploader({ url: 'upload_url' });
  hasBaseDropZoneOver: boolean = false;
  user: any;
  imgResponse: any;
  fileData: File = null;
  msg: string;
  imagePath;
  imgURL: any;

  message: string = 'Image Uploaded Successfully !';
  action: boolean = true;
  setAutoHide: boolean = true;
  autoHide: number = 4000;
  horizontalPosition: MatSnackBarHorizontalPosition = 'center';
  verticalPosition: MatSnackBarVerticalPosition = 'bottom';
  formData = new FormData();

  constructor(private dataservice: DataService,
    private router: Router,
    private apiservice: ApiService,
    private snack: MatSnackBar,
    private loader: AppLoaderService) {
    this.user = dataservice.getOption();

    let config = new MatSnackBarConfig();
    config.verticalPosition = this.verticalPosition;
    config.horizontalPosition = this.horizontalPosition;
    config.duration = this.setAutoHide ? this.autoHide : 0;
    // config.extraClasses = this.addExtraClass ? ['test'] : undefined;
  }

  back() {
    this.router.navigate(['profile/child']);
  }
  fileSelect(event) {

    this.fileData = event.target.files[0];
    this.formData.append('image', this.fileData);

    // --------------------preview image before upload ------------------------

    if (event.target.files.length === 0)
      return;
    var reader = new FileReader();
    this.imagePath = event.target.files;
    reader.readAsDataURL(event.target.files[0]);
    reader.onload = (_event) => {
      this.imgURL = reader.result;
    }
    var mimeType = event.target.files[0].type;
    if (mimeType.match(/image\/*/) == null) {
      this.msg = " only images are supported";
      return;
    }
    // -------------------------------------------------------------------------------

  }
  upload() {
    this.loader.open();
    this.apiservice.uploadUserImage(this.user._id, this.formData).subscribe(res => {
      this.imgResponse = res;
      this.loader.close();
      console.log('IMAGE Response from server = >>> ', this.imgResponse);
      if (this.imgResponse.isSuccess === true) {
        this.dataservice.setOption(this.imgResponse.data);
        this.snack.open(this.message, 'OK', { duration: 4000 });

        this.router.navigateByUrl('', { skipLocationChange: true }).then(() => {
          this.router.navigate(['profile/settings']);
        })

      } else {
        let msg = "Something Went Wrong!";
        this.snack.open(msg, 'OK', { duration: 4000 });

      }
    });
  }

  ngOnInit() {
  }
  public fileOverBase(e: any): void {
    this.hasBaseDropZoneOver = e;
  }

}
