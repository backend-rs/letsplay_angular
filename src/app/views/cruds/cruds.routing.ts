import { Routes } from '@angular/router';
import { CrudNgxTableComponent } from './crud-ngx-table/crud-ngx-table.component';
import { CrudProviderTableComponent } from './provider-provider-table/crud-provider-table.component';
import { CrudProgramTableComponent } from './crud-program-table/crud-program-table.component';
import { CrudChildTableComponent } from './crud-child-table/crud-child-table.component';
import { CrudParentTableComponent } from './crud-parent-table/crud-parent-table.component';

export const CrudsRoutes: Routes = [
  { 
    path: 'ngx-table', 
    component: CrudNgxTableComponent, 
    data: { title: 'NgX Table', breadcrumb: 'NgX Table' } 
  },
{ 
    path: 'parent', 
    component: CrudParentTableComponent, 
    data: { title: 'Parent', breadcrumb: 'PARENT' } 
  },
{ 
    path: 'provider', 
    component: CrudProviderTableComponent, 
    data: { title: 'Provider', breadcrumb: 'PROVIDER' } 
  },
{ 
    path: 'program', 
    component: CrudProgramTableComponent, 
    data: { title: 'Program', breadcrumb: 'PROGRAM' } 
  },
{ 
    path: 'child', 
    component: CrudChildTableComponent, 
    data: { title: 'Child', breadcrumb: 'CHILD' } 
  }
];
