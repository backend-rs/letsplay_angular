import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule } from '@angular/router';
import { ReactiveFormsModule } from '@angular/forms';
import { 
  MatInputModule,
  MatIconModule,
  MatCardModule,
  MatMenuModule,
  MatButtonModule,
  MatChipsModule,
  MatListModule,
  MatTooltipModule,
  MatDialogModule,
  MatSnackBarModule,
  MatSlideToggleModule
 } from '@angular/material';
import { FlexLayoutModule } from '@angular/flex-layout';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { SharedModule } from '../../shared/shared.module';
import { CrudNgxTableComponent, } from './crud-ngx-table/crud-ngx-table.component';

import { CrudsRoutes } from './cruds.routing';
import { CrudService } from './crud.service';
import { NgxTablePopupComponent } from './crud-ngx-table/ngx-table-popup/ngx-table-popup.component'
import { TranslateModule } from '@ngx-translate/core';
import { CrudChildTableComponent } from './crud-child-table/crud-child-table.component';
import { CrudParentTableComponent } from './crud-parent-table/crud-parent-table.component';
import { CrudProgramTableComponent } from './crud-program-table/crud-program-table.component';
import { CrudProviderTableComponent } from './provider-provider-table/crud-provider-table.component';
import { ChildTablePopupComponent } from './crud-child-table/child-table-popup/child-table-popup.component';
import { ParentTablePopupComponent } from './crud-parent-table/parent-table-popup/parent-table-popup.component';
import { ProviderTablePopupComponent } from './provider-provider-table/provider-table-popup/provider-table-popup.component';
import { ProgramTablePopupComponent } from './crud-program-table/program-table-popup/program-table-popup.component';

@NgModule({
  imports: [
    CommonModule,
    ReactiveFormsModule,
    FlexLayoutModule,
    NgxDatatableModule,
    MatInputModule,
    MatIconModule,
    MatCardModule,
    MatMenuModule,
    MatButtonModule,
    MatChipsModule,
    MatListModule,
    MatTooltipModule,
    MatDialogModule,
    MatSnackBarModule,
    MatSlideToggleModule,
    TranslateModule,
    SharedModule,
    RouterModule.forChild(CrudsRoutes)
  ],
  declarations: [CrudNgxTableComponent,
    CrudChildTableComponent,
    CrudParentTableComponent,
    CrudProgramTableComponent,
    CrudProviderTableComponent,
     NgxTablePopupComponent,
     ChildTablePopupComponent,
    ParentTablePopupComponent,
    ProviderTablePopupComponent,
    ProgramTablePopupComponent
    ],
  providers: [CrudService],
  entryComponents: [NgxTablePopupComponent,
    ChildTablePopupComponent,
    ParentTablePopupComponent,
    ProviderTablePopupComponent,
    ProgramTablePopupComponent]
})
export class CrudsModule { }
